<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{path?}', 'welcome')->where('path', '^((?!admin).)*$');
Auth::routes();

Route::prefix('admin')->group(function () {

    Route::get('/', function (Request $request) {
        return view('admin.index');
    })->name('admin.index')->middleware('auth');

    Route::get('/login', function (Request $request) {
        return view('admin.pages.auth.login');
    })->name('admin.login');
    Route::post('/login','AuthController@login');

    Route::resource('categories', 'Admin\CategoriesController')->middleware('auth');
    Route::resource('partner', 'Admin\PartnerController');
    Route::post('file/upload', 'FileController@upload')->name('file.upload');
    Route::resource('user', 'Admin\UserController');
    Route::resource('devises', 'Admin\DeviseController')->middleware('auth');
    Route::resource('etapes', 'Admin\EtapeController')->middleware('auth');
});
Route::get('pays', 'PaysController@CountriesList');
/*Route::post('/register','Auth\RegisterController@create');
Route::post('/register','AuthController@register');
Route::post('/login','AuthController@login');
Route::get('email/verify/{id}', 'VerificationEmailController@verify')->name('verification.verify');
Route::get('email/resend', 'VerificationEmailController@resend')->name('verification.resend');
Route::post('/create_diapo','DiaporamaController@create_diapo');*/


