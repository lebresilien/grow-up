<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
 Route::post('create', 'PasswordResetController@create');
 Route::get('find/{token}', 'PasswordResetController@find');
 Route::post('reset', 'PasswordResetController@reset');
 Route::get('find/{token}', 'PasswordResetController@find');

 Route::get('category', 'Admin\categoriesController@categories');
 Route::get('partner', 'Admin\PartnerController@partner');
 Route::get('pays', 'PaysController@CountriesList');

 Route::prefix('auth')->group(function ($route) {
    $route->post('/signin', 'AuthController@login');
    $route->post('/signup', 'AuthController@register');
    //$route->get('signup/activate/{token}', 'AuthController@signupActivate');
    $route->group(['middleware' => 'auth:api'], function($route) {
        $route->post('/user', 'AuthController@user');
        $route->post('/logout', 'AuthController@logout');
    });
    $route->group(['prefix' => 'password'], function($route) {
        $route->post('/create', 'PasswordResetController@create');
        $route->get('/find/{token}', 'PasswordResetController@find');
        $route->post('/reset', 'PasswordResetController@reset');
    });
});

/* Route::prefix('v1')->group(function ($route) {
    $route->group(['middleware' => 'auth:api'], function($route) {
        $route->put('/users/1', 'API\V1\UserController@update');
    });
    //$route->put('/users/1', 'API\V1\UserController@update');
});
 */
Route::middleware('auth:api')->prefix('v1')->namespace('API\V1')->group(function() {
    Route::apiResource('users', 'UserController');
    Route::post('users.logo', 'UserController@uploadLogo');
    Route::apiResource('conseillers', 'ConseillersController');
    Route::apiResource('projets', 'ProjectController');
    Route::post('projets.logo', 'ProjectController@uploadLogo');
    Route::post('projets.cover', 'ProjectController@uploadCover');
    
    Route::apiResource('equipes', 'EquipeController');
});

Route::middleware('api')->prefix('v1')->namespace('API\V1')->group(function() {
    Route::get('devises', 'DataController@devises');
    Route::get('categories', 'DataController@categories');
    Route::get('etapes', 'DataController@etapes');
    Route::post('projets.video', 'ProjectController@uploadVideo');
});