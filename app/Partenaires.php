<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partenaires extends Model implements HasMedia
{
    use InteractsWithMedia;
    
    public $timestamps = false;
    protected $fillable = [
        'name',
    ];
    protected $appends = ['image'];

    public function getImageAttribute()
    {
        if($this->getMedia('image')->first()) {
            $arrayLinks = explode("public", $this->getMedia('image')->first()->getPath());
            $link = Storage::url($arrayLinks[count($arrayLinks) - 1]);
        } else {
            $link = asset('images/defaults/category.png');
        }
        return $link;
    }
}
