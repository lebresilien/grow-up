<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Notifications\RegisteredNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable implements HasMedia 
{
    use  Notifiable, SoftDeletes, HasApiTokens, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','pays_residence', 'ville', 'contact','secteur_activite', 'contact_professionel', 'phone',
        'raison_sociale','name_organisation','pays_origine','last_name','identite', 'site_web_link', 'linkedin_link', 'facebook_link', 'twitter_link'
    ];

    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = ['logo'];

    public function sendEmailVerificationNotification()
    {
         $this->notify(new RegisteredNotification);
    }

    public function projets()
    {
     return $this->hasMany('App\Projets');
    }

    public function investir()
    {
     return $this->hasMany('App\Investissements');
    }

    public function demandes()
    {
     return $this->hasMany('App\Models\Demandes');
    }

    public function experiences()
    {
     return $this->hasMany('App\Models\Experience');
    }

    public function getLogoAttribute()
    {
        if($this->getMedia('logo')->first()) {
            $arrayLinks = explode("public", $this->getMedia('logo')->first()->getPath());
            $link = Storage::url($arrayLinks[count($arrayLinks) - 1]);
        }else{
            $link = asset('images/defaults/avatar.png');
        }
        return $link;
    }
}
