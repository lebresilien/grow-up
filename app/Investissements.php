<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investissements extends Model
{
     protected $fillable = [
        'user_id','montant','commentaire','status',
    ];

    public function projet()
    {
      return $this->belongsTo('App\Projets');
    }

    public function type()
    {
      return $this->belongsTo('App\TypeInvestissements');
    }
}
