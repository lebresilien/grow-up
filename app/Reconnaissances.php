<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reconnaissances extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name', 'description', 'projets_id','status',
    ];
}
