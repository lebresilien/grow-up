<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypesSouscriptions extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'libelle_type', 'status_type',
    ];

    public function investissements()
    {
    	return $this->hasMany('App\Investissements');
    }
}
