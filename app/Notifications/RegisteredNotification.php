<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegisteredNotification extends Notification implements ShouldQueue
{
	use Queueable;

   public function via($notifiable)
   {
      return ['mail'];
   }


    public function toMail($notifiable)
    {
      $url = url('/api/v1/auth/signup/activate/'.$notifiable->activation_token);    
      return (new MailMessage)
        ->subject('Confirmation compte')
        ->line('Merci pour votre inscription!.')
        ->action('Veuillez cliquer pour accéder à votre compte', url($url))
        ->line('Merci de nous faire confiance!');
      }
}
