<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membres extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'nom_membre', 'role_membre', 'photo_membre','statut_membre',
    ];
}
