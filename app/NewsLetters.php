<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsLetters extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'user_id','created_at'
    ];
}
