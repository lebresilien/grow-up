<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diaporamas extends Model
{
    protected $fillable = [
        'titre', 'image_diapo','status',
    ];
}
