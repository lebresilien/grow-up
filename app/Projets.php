<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projets extends Model
{
    protected $fillable = [
        'nom','numero_contribuable','rccm','logo','statut_projet','presentation_video','cout_projet','document_reglementaire','document_reglementaire',
        'brochure','presentation_projet','plan_developpement','repartition_capital','plan_sortie','user_id','categories_id','debut_levee','fin_levee','status',
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function categorie()
    {
      return $this->belongsTo('App\Categories');
    }

    public function membres()
    {
      return $this->hasMany('App\Membres');
    }

    public function investissements()
    {
      return $this->hasMany('App\Investissements');
    }	

}
