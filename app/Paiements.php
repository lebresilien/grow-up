<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paiements extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'libelle_paiement','logo_paiement','status_paiement',
    ];	
}
