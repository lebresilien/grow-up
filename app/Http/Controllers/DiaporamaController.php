<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gestion\PhotoGestionInterface;
use App\Repositories\DiaporamaRepository;
use Validator;
use Response;

class DiaporamaController extends Controller
{
	protected  $diapoRepository;

	public function __construct(DiaporamaRepository $diaporamaRepository)
    {  
        $this->diapoRepository = $diaporamaRepository;
    }

    protected $rules =
    [
        'titre' => 'required|string',
        'image_diapo' => 'required|image'
    ];
    
    public function create_diapo(Request $request, PhotoGestionInterface $photoGestion)
    {
      $validator = Validator::make($request->all() , $this->rules);

      if($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else
        {
          $nom = $photoGestion->save($request->file('image'));
          if($nom){
            $retour = $this->diaporamaRepository->create($request->titre,$nom);
            return response()->json($retour);
           }
        }
    }
}
