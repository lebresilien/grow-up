<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Conseiller;
use Illuminate\Http\Request;

class ConseillerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conseillers = Conseiller::all();
        return view('admin.conseillers.index', compact('conseillers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $conseiller = new Conseiller();
        return view('admin.conseillers.create', compact('conseiller'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate(array(
            'name' => 'required|min:3|unique:conseillers',
            'email' => 'required|string|email|unique:conseillers',
        ));

        $etape = new Conseiller();
        $etape->name = $request->input('name');
        $etape->name = $request->input('email');
        $etape->projets_id = $request->input('projets_id');
        $categories->save();

        return redirect(route('conseillers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conseiller  $conseiller
     * @return \Illuminate\Http\Response
     */
    public function show(Conseiller $conseiller)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conseiller  $conseiller
     * @return \Illuminate\Http\Response
     */
    public function edit(Conseiller $conseiller)
    {
        return view('admin.conseillers.edit', compact('conseiller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conseiller  $conseiller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conseiller $conseiller)
    {
        $conseiller = Conseiller::findOrFail($conseiller->id);
        $conseiller->name = $request->input('name');
        $category->save();

        return redirect(route('conseillers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conseiller  $conseiller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conseiller $conseiller)
    {
        $conseiller = Conseiller::find($conseiller->id);
        $conseiller->delete();
        return  redirect()->route('conseillers.index');
    }
}
