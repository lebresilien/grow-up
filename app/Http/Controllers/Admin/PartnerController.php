<?php

namespace App\Http\Controllers\Admin;

use App\Partenaires;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partenaires::all();
        return view('admin.partenaires.index', compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partner = new Partenaires();
        return view('admin.partenaires.create', compact('partner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate(array(
            'name' => 'required|min:3|unique:partenaires',
        ));

        $partner = new Partenaires();
        $partner->name = $request->input('name');
        $partner->save();

        foreach ($request->input('document', []) as $file) {
            $partner->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('image');
        }

        return redirect(route('partner.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partenaires  $partenaires
     * @return \Illuminate\Http\Response
     */
    public function show(Partenaires $partenaires)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partenaires  $partenaires
     * @return \Illuminate\Http\Response
     */
    public function edit(Partenaires $partner)
    {
        return view('admin.partenaires.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partenaires  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partenaires $partner)
    {
        $partner = Partenaires::findOrFail($partner->id);
        
        if (count($partner->getMedia('image')) > 0) {
            foreach ($partner->getMedia('image') as $media) {
                if (!in_array($media->file_name, $request->input('document', []))) {
                    $media->delete();
                }
            }
        }
        
        $media = $partner->getMedia('image')->pluck('file_name')->toArray();

        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $partner->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('image');
            }
        }

        $partner->name = $request->input('name');
        $partner->save();

        return redirect(route('partner.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partenaires  $partenaires
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partenaires $partner)
    {
        $partner = Partenaires::find($partner->id);
        $partner->delete();
        return  redirect()->route('partner.index');
    }

    public function partner()
    {
        $partners = Partenaires::all();
        return response()->json($partners);
    }
}
