<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Etape;
use Illuminate\Http\Request;

class EtapeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $etapes = Etape::all();
        return view('admin.etapes.index', compact('etapes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $etape = new Etape();
        return view('admin.etapes.create', compact('etape'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate(array(
            'name' => 'required|min:3|unique:etapes'
        ));

        $etape = new Etape();
        $etape->name = $request->input('name');
        $etape->save();

        return redirect(route('etapes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Etape  $etape
     * @return \Illuminate\Http\Response
     */
    public function show(Etape $etape)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Etape  $etape
     * @return \Illuminate\Http\Response
     */
    public function edit(Etape $etape)
    {
        return view('admin.etapes.edit', compact('etape'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Etape  $etape
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Etape $etape)
    {
        $etape = Etape::findOrFail($etape->id);
        $etape->name = $request->input('name');
        $etape->save();

        return redirect(route('etapes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Etape  $etape
     * @return \Illuminate\Http\Response
     */
    public function destroy(Etape $etape)
    {
        $etape = Etape::find($etape->id);
        $etape->delete();
        return  redirect()->route('etapes.index');
    }
}
