<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = User::with('experiences')->where('id',$user->id)->get();
        return response()->json($user, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    { 
        //$this->authorize('update', $user);
        $validation = $request->validate(array(
            'email' => 'required|string|email|unique:users',
            'lieu' => 'required|string|min:6',
        ));

        $user = User::find($user->id);
        isset($request->name) ? $user->name = $request->name : '';
        isset($request->last_name) ? $user->last_name = $request->last_name : '';
        isset($request->identite) ? $user->identite = $request->identite : '';
        isset($request->localisation) ? $user->localisation = $request->localisation : '';
        isset($request->bio) ? $user->bio = $request->bio : '';
        isset($request->site_url) ? $user->site_web_link = $request->site_url : '';
        isset($request->facebook_link) ? $user->facebook_link = $request->facebook_link : '';
        isset($request->twitter_link) ? $user->twitter_link = $request->twitter_link : '';
        isset($request->linked_link) ? $user->linkedin_link = $request->linked_link : '';
        isset($request->email) ? $user->linkedin_link = $request->email : '';
        isset($request->phone) ? $user->phone = $request->phone : '';
         
        
        if($request->filled('logo')){
            //$user->addMedia(storage_path($request->logo))->toMediaCollection('logo');  
            return response()->json('oui', 200);
        }

        if ($request->hasFile('logo')) {
            return response()->json('test', 200);
        }
        
        if($request->filled('experience')){

            $exp = Experience::where('user_id', $user->id)->delete();
            
            foreach($request->experience as $value){
                $exp = Experience::create(['name' => $value['name'],'role' => $value['role'],'started_year' => $value['started_year'],'ended_year' => $value['ended_year'],'user_id' => $user->id]); 
            }  

            return response()->json('yes', 201);
        }
        
        $user->save();
        return response()->json($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        
    }

    public function uploadLogo(Request $request)
    {
        $user = User::find(auth('api')->user()->id);
        $path = storage_path('app/public/logo');

        if(count($user->getMedia('logo')) > 0) {
            foreach ($user->getMedia('logo') as $media) { 
              $media->delete();   
            }
        }

        $file = $request->file;
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        $user->addMedia(storage_path('app\public\logo\\' . $name ))->toMediaCollection('logo');
        $user = User::find(auth('api')->user()->id);
        
        return response()->json($user, 200);
        
    }
}
