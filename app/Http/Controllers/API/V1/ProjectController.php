<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Projet;
use App\Models\Equipe;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Projet::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate(array(
            'name' => 'required|min:3|unique:categories',
            'concept' => 'required|string|min:120',
            'local' => 'required|string|min:6',
            'categories_id' => 'required|integer',
            'etapes_id' => 'required|integer',
            'devises_id' => 'required|integer',
        ));

        $projet = new Projet();
        $projet->name = $request->name;
        $projet->slug = Str::slug($request->name);
        $projet->concept = $request->concept;
        $projet->localisation = $request->local;
        $projet->devises_id = $request->devises_id;
        $projet->etapes_id = $request->etapes_id;
        $projet->categories_id = $request->categories_id;
        $projet->user_id = auth('api')->user()->id;

        $projet->save();
        return response()->json($projet, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Projet  $projet
     * @return \Illuminate\Http\Response
     */
    public function show(Projet $projet)
    {
        $project = Projet::with(['etape', 'devise', 'category', 'equipes', 'user'])->where('id',$projet->id)->get();
        return response()->json($project, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Projet  $projet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Projet $projet)
    {
        /* $validation = $request->validate(array(
            'name' => 'required|min:3|unique:categories',
            'spitch' => 'required|string|min:30',
            'lieu' => 'required|string|min:6',
        )); */

        $projet = Projet::find($projet->id);
        isset($request->name) ? [$projet->name = $request->name, $projet->slug = Str::slug($request->name)] : '';
        isset($request->concept) ? $projet->concept = $request->concept : '';
        isset($request->spitch) ? $projet->spitch = $request->spitch : '';
        isset($request->lieu) ? $projet->localisation = $request->lieu : '';
        isset($request->created_at) ? $projet->etse_created_date = $request->created_at : '';
        isset($request->nbre_employe) ? $projet->nbre_employe = $request->nbre_employe : '';
        isset($request->site_web_link) ? $projet->site_web_link = $request->site_web_link : '';
        isset($request->facebook_link) ? $projet->facebook_link = $request->facebook_link : '';
        isset($request->twitter_link) ? $projet->twitter_link = $request->twitter_link : '';
        isset($request->linkedin_link) ? $projet->linkedin_link = $request->linkedin_link : '';
        isset($request->problematique) ? $projet->problematique = $request->problematique : '';
        isset($request->product_service) ? $projet->product_service = $request->product_service : '';
        isset($request->market_target) ? $projet->market_target = $request->market_target : '';
        isset($request->business_model) ? $projet->business_model = $request->business_model : '';
        isset($request->client) ? $projet->client = $request->client : '';
        isset($request->sale_marketing_strategy) ? $projet->sale_marketing_strategy = $request->sale_marketing_strategy : '';
        isset($request->concurrent) ? $projet->concurrent = $request->concurrent : '';
        isset($request->avantage_concurrentiel) ? $projet->avantage_concurrentiel = $request->avantage_concurrentiel : '';
        isset($request->amount) ? $projet->amount = $request->amount : '';
        isset($request->souscription_amount) ? $projet->souscription_amount = $request->souscription_amount : '';
        isset($request->levee_start) ? $projet->levee_start = $request->levee_start : '';
        isset($request->levee_end) ? $projet->levee_end = $request->levee_end : '';
        isset($request->numero_contribuable) ? $projet->etse_created_date = $request->etse_created_date : '';
        isset($request->rccm) ? $projet->rccm = $request->rccm : '';
        isset($request->status_projet) ? $projet->status_projet = $request->status_projet : '';
        isset($request->category_id) ? $projet->categories_id = $request->category_id : '';
        isset($request->etape_id) ? $projet->etapes_id = $request->etape_id : '';
        isset($request->devise_id) ? $projet->devises_id = $request->devise_id : '';
        isset($request->equipe_direction) ? $projet->equipe_direction = $request->equipe_direction : '';

        if($request->filled('expertise')){

            $equipe = Equipe::where('projets_id', $projet->id)->delete();
            
            foreach($request->expertise as $value){
                $equipes = Equipe::create(['name' => $value['name'],'role' => $value['role'],'expertise' => $value['expertise'],'email' => $value['email'],'projets_id' => $projet->id]); 
            }  

            return response()->json($equipes, 201);
        }

        $projet->save();
        return response()->json($projet, 200);
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Projet  $projet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Projet $projet)
    {
        //
    }

    public function uploadLogo(Request $request)
    {
        $project = Projet::find($request->id);
        $path = storage_path('app/public');

        if(count($project->getMedia('logo')) > 0) {
            foreach ($project->getMedia('logo') as $media) { 
              $media->delete();   
            }
        }
 
        $file = $request->file;
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        $project->addMedia($path . '/'. $name)->toMediaCollection('logo');
        $project = Projet::find($request->id);
        return response()->json($project, 200); 
        
    }

    public function uploadCover(Request $request)
    {
        $project = Projet::find($request->id);
        $path = storage_path('app/public');

        if(count($project->getMedia('cover')) > 0) {
            foreach ($project->getMedia('cover') as $media) { 
              $media->delete();   
            }
        }
 
        $file = $request->file;
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        $project->addMedia($path . '/'. $name)->toMediaCollection('cover');
        $project = Projet::find($request->id);
        return response()->json($project, 200); 
        
    }

    public function uploadVideo(Request $request)
    {
        $project = Projet::find($request->id);
        $path = storage_path('app/public');

        if(count($project->getMedia('video')) > 0) {
            foreach ($project->getMedia('video') as $media) { 
              $media->delete();   
            }
        }
 
        $file = $request->file;
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        $project->addMedia($path . '/'. $name)->toMediaCollection('video');
        $project = Projet::find($request->id);
        return response()->json($project, 200); 
        
    }

    public function uploadFile(Request $request)
    {
        $type = $request->type;
        $project = Projet::find($request->id);
        $path = storage_path('app/public');

        if(count($project->getMedia(''.$type.'')) > 0) {
            foreach ($project->getMedia(''.$type.'') as $media) { 
              $media->delete();   
            }
        }
 
        $file = $request->file;
        $name = uniqid() . '_' . trim($file->getClientOriginalName());
        $file->move($path, $name);

        $project->addMedia($path . '/'. $name)->toMediaCollection(''.$type.'');
        $project = Projet::find($request->id);
        return response()->json($project, 200); 
        
    }
}
