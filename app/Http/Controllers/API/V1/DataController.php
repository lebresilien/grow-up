<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Etape;
use App\Models\Devise;

class DataController extends Controller
{
    public function categories()
    {
       return Categories::all();
    }

    public function devises()
    {
       return Devise::all();
    }

    public function etapes()
    {
       return Etape::all();
    }
}
