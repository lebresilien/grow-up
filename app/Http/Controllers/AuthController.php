<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Notifications\RegisteredNotification;
use Illuminate\Support\Str;
use Response;
use Validator;

class AuthController extends Controller
{

    protected $rules =
    [
        'name' => 'required|string',
        'email' => 'required|string|email|unique:users',
        'password' => 'required|string|min:8',
        'pays_origine' => 'required|string',
        'pays_residence' => 'required|string',
        'use_conditions' => 'required',
        'investment' => 'required',
    ];

    public function register(Request $request)
    {
        $validator = Validator::make($request->all() , $this->rules);
		 if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        }else {

                $user = new User([
                                  'name' => $request->name,
                                  'email' => $request->email,
                                  'last_name' => $request->last_name,
                                  'identite' => $request->identite,
                                  'pays_origine' => $request->pays_origine,
                                  'pays_residence' => $request->pays_residence,
                                  'activation_token' => Str::random(60),
                                  'password' => bcrypt($request->password)
                               ]);

                $user->save();
                //$user->notify(new RegisteredNotification($user));
                
                //$user = $request->user();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;

                return response()->json([
                                         'success' => true,
                                         'id' => $user->id,
                                         'name' => $user->name,
                                         'email' => $user->email,
                                         'token_type' => 'Bearer',
                                         'access_token' => $tokenResult->accessToken,
                                        ], 201);
        } 
        
    }

    public function login(Request $request)
    {

        /*$request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);*/

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Paramétres de connexion incorrets'
            ]);

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'success' => true,
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 201);
    }

    
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function signupActivate($token)
    {
       $user = User::where('activation_token', $token)->first();
       if (!$user) {
        return response()->json([
            'message' => 'This activation token is invalid.'
        ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        return $user;
    }
}
