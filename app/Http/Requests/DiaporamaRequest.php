<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiaporamaRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'titre' => 'required|string',
            'image_diapo' => 'required|image'
        ];
    }
}
