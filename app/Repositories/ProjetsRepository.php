<?php

namespace App\Repositories;
use App\Projets;
use App\Investissements;

class ProjetsRepository extends ResourceRepository
{
    
    public function __construct(Projets $projets)
    {  
        $this->model = $projets;
    }
    
    //recuperer la categorie d'un projet
    public function projet_categorie($id_projet)
    {
    	return Projets::find($id_projet)->categorie;
    }

    //tous les projets en campagne
    public function projets_campagne()
    {
      return Projets::where('status_projet', 0)
                      ->orderBy('nom', 'desc')
                      ->get();
    }

    //tous les projets  cloturés
    public function projets_close()
    {
      return Projets::where('status_projet', 2)
                      ->orderBy('nom', 'desc')
                      ->get();
    }

    //tous les projets en financement
    public function projets_finance()
    {
      return Projets::where('status_projet', 1)
                      ->orderBy('nom', 'desc')
                      ->get();
    }

    //faire passer une projet du statut campagne à financé
    public function changeToFinancer($id_projet)
    {
      return Projets::where('id', $id_projet)
                      ->update(['status_projet' => 1]);
    }

    //faire passer une projet du statut finacé à cloturé
    public function changeToCloturer($id_projet)
    {
      return Projets::where('id', $id_projet)
                      ->update(['status_projet' => 2]);
    }

    //requete qui recupere tous les projets en fontion du status pour les utilisateurs
    public function projets_statut_user($last_id,$status)
    {
        if($last_id  > 0 )
        {
           return $projets = DB::table('projets')->select('projets.id as id', 'name', 'photo', 'logo', 'nom', 'defis', 'montant_en_cours', 'cout_projet', 'categories_id', 'debut_levee','fin_levee')->join('users', 'projets.user_id', '=', 'users.id')->where([['projets.id','<',$last_id],['status_projet', $status],['status', 1]])->orderBy('projets.id','DESC')->limit(8)->get(); 
           
       }else if($last_id == '')
       {
         return $projets = DB::table('projets')->select('projets.id as id', 'name', 'photo', 'logo', 'nom', 'defis', 'montant_en_cours', 'cout_projet', 'categories_id', 'debut_levee','fin_levee')->join('users', 'projets.user_id', '=', 'users.id')->where([['status_projet', $status],['status', 1]])->orderBy('projets.id','DESC')->limit(8)->get();
       }else
       {
        return 'empty';
       }
        
    }

    //requete qui recupere tous les projets en fonction du status pour les admin
    public function projets_status_admin($status)
    {
       return $projets = DB::table('projets')->select('projets.id as id', 'name', 'photo', 'logo', 'nom', 'defis', 'montant_en_cours', 'cout_projet', 'categories_id', 'debut_levee','fin_levee')->join('users', 'projets.user_id', '=', 'users.id')->where([['projets.id','<',$last_id],['status_projet', $status],['status', 1]])->orderBy('projets.id','DESC')->paginate(10); 
    }

    //investir sur un projet
    public function investissement($id_projet,$id_user,$montant,$commentaire,$type)
    {
        $invest = new Investissements;

        $invest->user_id = $id_user;
        $invest->projet_id = $id_projet;
        $invest->user_id = $id_user;
        $invest->types_souscriptions_id = $type;
        $invest->commentaire = $commentaire;

        if($invest->save())
        {
           return DB::table('projets')
                  ->increment('montant_en_cours', $montant)
                  ->where('id', $id_projet);
        }else
        {
            return response()->json('error');
        }

    }

    //recuperer tous les investissements d'un projet
    public function projet_investment($id_projet)
    {
        return $projets = DB::table('investissements')->select('montant', 'commentaire', 'nom')->join('projets', 'investissements.projet_id', '=', ' projets.id')->where('projet_id', $id_projet)->get();
    }

    

}