<?php

namespace App\Repositories;
use App\User;

class UserRepository extends ResourceRepository
{
    
    public function __construct(User $user)
    {  
        $this->model = $user;
    }
    
    //recuperer tous les  projets d'un utilisateur
    public function user_project($id_user)
    {
    	return User::find($id_user)->projets;
    }

    // tous les utilisateurs en fonction du role
    public function userByRole($id_role)
    {
      
    }

}