<?php

namespace App\Repositories;
use App\Partenaires;

class PartenaireRepository extends ResourceRepository
{
    
    public function __construct(Partenaires $partenaires)
    {  
        $this->model = $partenaires;
    }

}