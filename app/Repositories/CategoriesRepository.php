<?php

namespace App\Repositories;
use App\Categories;

class ClassesRepository extends ResourceRepository
{
    
    public function __construct(Categories $category)
    {  
        $this->model = $category;
    }
   
    //les projets appartenants à une categorie
    public function category_projects($id_cat)
    {
    	return Categories::find($id_cat)->projetss;
    }

}