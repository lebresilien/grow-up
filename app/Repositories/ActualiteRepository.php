<?php

namespace App\Repositories;
use App\Actualites;

class ActualiteRepository extends ResourceRepository
{
    
    public function __construct(Actualites $actualites)
    {  
        $this->model = $actualites;
    }


}