<?php

namespace App\Repositories;
use App\Diaporamas;

class DiaporamaRepository extends ResourceRepository
{
    
    public function __construct(Diaporamas $diaporama)
    {  
        $this->model = $diaporama;
    }

    public function create($titre,$nom_image)
    {
       $diapo = new Diaporamas;

       $diapo->titre = $titre;
       $diapo->image_diapo = $nom_image;
       return save($diapo); 
    }


}