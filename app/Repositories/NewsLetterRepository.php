<?php

namespace App\Repositories;
use App\NewsLetter;

class NewsLetterRepository extends ResourceRepository
{
    
    public function __construct(NewsLetter $news)
    {  
        $this->model = $news;
    }


}