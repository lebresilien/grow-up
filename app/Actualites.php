<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actualites extends Model
{
	public $timestamps = false;	
    protected $fillable = [
        'libelle_actualite', 'logo_actualite','status_actualite',
    ];
}
