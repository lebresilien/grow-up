<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    public $timestamps = false;
      
    protected $fillable = [
        'name', 'role', 'started_year', 'ended_year', 'user_id'
    ];

    public function user(){
        return $this->belognsTo('App\User');
    }
}
