<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'role', 'projets_id', 'email', 'expertise'];

   
}
