<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Demande extends Model
{
    protected $fillable = [
        'name', 'description', 'user_id','categories_id', 
    ];

    public function user(){
        return $this->belognsTo('App\User');
    }

    public function category(){
        return $this->belognsTo('App\Models\Categories');
    }
}
