<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conseiller extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'email', 'projects_id'];
}
