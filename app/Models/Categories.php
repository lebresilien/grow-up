<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model implements HasMedia
{
    use InteractsWithMedia, SoftDeletes;

    public $timestamps = false;
      
    protected $fillable = [
        'name', 'description',
    ];
    protected $appends = ['image'];

    public function projets()
    {
      return $this->hasMany('App\Models\Projet');
    }

    public function demandes()
    {
      return $this->hasMany('App\Demandes');
    }


    public function getImageAttribute()
    {
        if($this->getMedia('image')->first()) {
            $arrayLinks = explode("public", $this->getMedia('image')->first()->getPath());
            $link = Storage::url($arrayLinks[count($arrayLinks) - 1]);
        } else {
            $link = asset('images/defaults/category.png');
        }
        return $link;
    }
}
