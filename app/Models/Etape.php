<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etape extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    
    public function projects()
    {  
      return $this->hasMany('App\Models\Projects');   
    }
}
