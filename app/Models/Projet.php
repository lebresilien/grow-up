<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Categories;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Support\Facades\Storage;

class Projet extends Model implements HasMedia
{
    use InteractsWithMedia;
    protected $fillable = ['name', 'slug', 'spitch', 'concept', 'localisation', 'user_id', 'categories_id', 'etapes_id', 'devises_id',
    'etse_created_date', 'nbre_employe', 'site_web_link', 'facebook_link', 'linkedin_link', 'twitter_link', 'problematique', 'product_service', 'market_target',
    'business_model', 'client', 'sale_marketing_strategy', 'concurrent', 'amount', 'souscription_amount', 'levee_start', 'levee_end', 'numero_contribuable',
    'rccm', 'status_projet'];

    protected $appends = ['logo', 'cover', 'video', 'presentation'];

    public function etape()
    {
        return $this->belongsTo('App\Models\Etape', 'etapes_id');
    }

    public function devise()
    {
        return $this->belongsTo('App\Models\Devise', 'devises_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Categories','categories_id');
    }

    public function equipes()
    {
        return $this->hasMany('App\Models\Equipe','projets_id');
    }

    public function conseillers()
    {
        return $this->hasMany('App\Models\Conseiller', 'projets_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getLogoAttribute()
    {
        if($this->getMedia('logo')->first()) {
            $arrayLinks = explode("public", $this->getMedia('logo')->first()->getPath());
            $link = Storage::url($arrayLinks[count($arrayLinks) - 1]);
        }else{
            $link = asset('images/defaults/category.png');
        }
        return $link;
    }

    public function getCoverAttribute()
    {
        if($this->getMedia('cover')->first()) {
            $arrayLinks = explode("public", $this->getMedia('cover')->first()->getPath());
            $link = Storage::url($arrayLinks[count($arrayLinks) - 1]);
        }else{
            $link = asset('images/defaults/background.jpg');
        }
        return $link;
    }

    public function getVideoAttribute()
    {
        if($this->getMedia('video')->first()) {
            $arrayLinks = explode("public", $this->getMedia('video')->first()->getPath());
            $link = Storage::url($arrayLinks[count($arrayLinks) - 1]);
        }else{
            $link = '';
        }
        return $link;
    }
}
