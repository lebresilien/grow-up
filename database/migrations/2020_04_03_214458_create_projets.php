<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->text('spitch')->nullable();
            $table->text('concept');
            $table->string('localisation');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->foreignId('categories_id')->constrained()->onDelete('cascade');
            $table->foreignId('etapes_id')->constrained()->onDelete('cascade');
            $table->foreignId('devises_id')->constrained()->onDelete('cascade');
            $table->date('etse_created_date')->nullable();
            $table->integer('nbre_employe')->nullable();
            $table->string('site_web_link')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->longText('equipe_direction')->nullable();
            $table->longText('problematique')->nullable();
            $table->longText('product_service')->nullable();
            $table->longText('market_target')->nullable();
            $table->longText('business_model')->nullable();
            $table->longText('client')->nullable();
            $table->longText('sale_marketing_strategy')->nullable();
            $table->longText('concurrent')->nullable();
            $table->longText('avantage_concurrentiel')->nullable();
            $table->double('amount')->nullable();
            $table->double('souscription_amount', 8, 2)->nullable();
            $table->date('levee_start')->nullable();
            $table->date('levee_end')->nullable();
            $table->string('numero_contribuable')->nullable();
            $table->string('rccm')->nullable();
            $table->integer('status_projet')->default(0);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projets');
    }
}
