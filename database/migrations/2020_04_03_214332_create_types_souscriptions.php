<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypesSouscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types_souscriptions', function (Blueprint $table) {
            $table->id();
            $table->string('libelle_type');
            $table->boolean('status_type')->default(true);
        });
    }

    public function down()
    {
        Schema::dropIfExists('types_souscriptions');
    }
}
