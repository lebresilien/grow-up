<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name')->nullable();
            $table->string('identite')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('pays_origine')->nullable();
            $table->string('pays_residence')->nullable();
            $table->string('ville')->nullable();
            $table->string('contact')->nullable();
            $table->string('site_web_link')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('secteur_activite')->nullable();
            $table->string('contact_professionel')->nullable();
            $table->string('localisation')->nullable();
            $table->longText('bio')->nullable();
            $table->string('raison_sociale')->nullable();
            $table->string('name_organisation')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('activated')->default(false);
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
