<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Demande de creation de projet</h2>
    <p>Réception d'une demande de crèation de projet avec les elements suivants :</p>
    <ul>
      <li><strong>Nom du Porteur de Projet</strong> : {{ $user['nom'] }}</li>
      <li><strong>Email</strong> : {{ $user['email'] }}</li>
      <li><strong>Nom du Projet</strong> : {{ $user['name'] }}</li>
      <li><strong>Categorie du Projet</strong> : {{ $user['categorie'] }}</li>
      <li><strong>Description du Projet</strong> : {{ $user['description'] }}</li>
    </ul>
  </body>
</html