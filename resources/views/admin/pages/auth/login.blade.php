@extends('admin.pages.auth.layouts.app')
@section('title', 'Login')
@section('content')
{!! Form::open(['url' => 'login', 'class'=>'needs-validation', 'novalidate'=>'']) !!}
                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::email('email', null, ['class' => 'form-control '.( $errors->has('email')), 'required' => 'required', 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your email
                        </div>
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                  <div class="form-group">
                    <div class="d-block">
                         {!! Form::label('password', 'Password', ['class' => 'control-label']) !!}
                      <div class="float-right">
                        <a href="#" class="text-small">
                          Forgot Password?
                        </a>
                      </div>
                    </div>
                    {!! Form::password('password', ['class' => 'form-control '.( $errors->has('password')), 'required' => 'required', 'tabindex'=>"2"] ) !!}
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                    @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      {!! Form::checkbox("remember", null, old('remember') ? 'checked' : '', ['class' => 'custom-control-input', 'tabindex' =>'3', 'id' =>'remember']) !!}
                      {!! Form::label('remember', 'Remember Me', ['class' => 'custom-control-label']) !!}
                    </div>
                  </div>

                  <div class="form-group">
                    {!! Form::submit('Login', ['class' => 'btn btn-primary btn-lg btn-block', 'tabindex'=>'4']) !!}
                  </div>
                {!! Form::close() !!}
@endsection