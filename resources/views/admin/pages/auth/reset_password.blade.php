@extends('admin.pages.auth.layouts.app')
@section('title', 'Reset Password')
@section('content')
<p class="text-muted">We will send a link to reset your password</p>
{!! Form::open(['url' => route('password.update'), 'class'=>'needs-validation', 'novalidate'=>'']) !!}
                    <div class="form-group">
                        {!! Form::hidden('token', $token,) !!}
                      
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::email('email', $email ?? old('email'), ['class' => 'form-control '.( $errors->has('email')), 'required' => 'required', 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
                        <div class="invalid-feedback">
                            Please fill in your email
                        </div>
                        @error('email')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                  <div class="form-group">
                    <div class="d-block">
                         {!! Form::label('password', 'New Password', ['class' => 'control-label']) !!}
                    </div>
                    {!! Form::password('password', ['class' => 'form-control '.( $errors->has('password')), 'required' => 'required', 'tabindex'=>"2"] ) !!}
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                    @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <div class="d-block">
                         {!! Form::label('password_confirmation', 'Confirm Password', ['class' => 'control-label']) !!}
                    </div>
                    {!! Form::password('password_confirmation', ['class' => 'form-control '.( $errors->has('password_confirmation')), 'required' => 'required', 'tabindex'=>"3"] ) !!}
                    <div class="invalid-feedback">
                      please fill in your password confirmation
                    </div>
                    @error('password_confirmation')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                  </div>

                  <div class="form-group">
                    {!! Form::submit('Login', ['class' => 'btn btn-primary btn-lg btn-block', 'tabindex'=>'4']) !!}
                  </div>
                {!! Form::close() !!}
@endsection