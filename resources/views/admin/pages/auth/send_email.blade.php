@extends('admin.pages.auth.layouts.app')
@section('title', 'Forgot Password')
@section('content')
<p class="text-muted">We will send a link to reset your password</p>
{!! Form::open(['url' => route('password.email'), 'class'=>'needs-validation', 'novalidate'=>'']) !!}
  <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control '.( $errors->has('email')), 'required' => 'required', 'tabindex'=>"1", "autofocus"=>"true"] ) !!}
        <div class="invalid-feedback">
            Please fill in your email
        </div>
        @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
    </div>

  <div class="form-group">
    {!! Form::submit('Forgot Password', ['class' => 'btn btn-primary btn-lg btn-block', 'tabindex'=>'2']) !!}
  </div>
{!! Form::close() !!}
@endsection