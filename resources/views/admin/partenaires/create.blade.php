@extends('admin.layouts.app')
@section('title', 'Partenaire')
@section('content')

<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Creation Partenaire</h1>
          </div>
          <div class="section-body">
            <div class="row">
              <div class="mx-auto">
                <div class="card-body">
                  @include('admin.partenaires.form')
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

@endsection
