<?php
if ($partner->id) {
    $options = ['method' => 'put', 'class'=>'needs-validation',  'url' => action('Admin\PartnerController@update', $partner), 'files' => true];
} else {
    $options = ['method' => 'post', 'class'=>'needs-validation', 'url' => action('Admin\PartnerController@store'), 'files' => true];
}
?>


@include('utilities.errors')
@include('utilities.flash')
@section('styles')
    {{-- Dropzone js cdn --}}
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/dropzone/dist/min/dropzone.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/croppie.css') }}" />
@endsection
{!! Form::model($partner, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf
<div class="card">
    <div class="card-header">
        <h4>Ajouter un partenaire</h4>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="form-group">
            <label for="document">Image (Importer une seule image)</label>
            <div class="dropzone" id="document-dropzone">
                <div class="fallback">
                    <input name="file" type="file" multiple/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Name or title of the category..."]) !!}
        </div>
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 
@section('scripts')
    <script src="{{ asset('plugins/dropzone/dist/dropzone.js') }}"></script>
    <script src="{{ asset('js/croppie.js') }}"></script>
    @include('utilities.dropzone', ['model' => $partner]);
@endsection



