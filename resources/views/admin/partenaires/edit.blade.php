@extends('admin.layouts.app')
@section('title', 'Categories')

@section('content')
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Edit Category</h1>
          </div>
          <div class="section-body">
            <div class="row">
              <div class=" mx-auto">
                <div class="card">
                  <div class="card-header">
                    <h4>Default Tab</h4>
                  </div>
                  <div class="card-body">
                    <div  id="myTabContent">
                      <div class="fade show active" id="generale" role="tabpanel" aria-labelledby="generale-tab">  
                        @include('admin.categories.form')
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection


