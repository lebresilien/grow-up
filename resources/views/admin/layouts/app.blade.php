<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="google-signin-client_id" content="277757494685-it24lhoh4esc227coe27rp2ero8kbq17.apps.googleusercontent.com">
    <title>@yield('title', config('app.name'))</title>
    @include('admin.layouts.partials.include')
    @stack('base_link')
</head>

<body>
  <div id="app">
    <div class="main-wrapper">
      @include('admin.layouts.partials.navbar')
      @include('admin.layouts.partials.sidebar')

      <!-- Main Content -->
      @yield('content')
      @include('admin.layouts.partials.footer')
    </div>
  </div>
  @stack('base_script')
  @yield('scripts')
</body>
</html>
