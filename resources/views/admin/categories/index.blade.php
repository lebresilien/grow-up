@extends('admin.layouts.app')
@section('title', 'Categories')
@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Listing Categories</h1>
          </div>
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Basic DataTables</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @forelse($categories as $category)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $category->name }}</td>
                              <td class="text-break">{{ $category->description }}</td>
                              
                              <td>
                                <a class="btn btn-primary btn-action mr-1" href="{{ route('categories.edit', $category) }}" data-toggle="tooltip"  data-original-title="Edit">
                                  <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a class="btn btn-primary btn-action mr-1" href="{{ route('categories.show', $category) }}" data-toggle="tooltip"  data-original-title="Show">
                                  <i class="fas fa-pencil-alt"></i>
                                </a>
                                {{--<a class="btn btn-danger btn-action trigger--fire-modal-1" 
                                  data-toggle="tooltip" title="" 
                                  data-confirm="Are You Sure?|This action can not be undone. Do you want to continue?" 
                                  data-confirm-yes="alert('Deleted')" data-original-title="Delete">
                                  <i class="fas fa-trash"></i>
                                </a>--}}
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => route('categories.destroy', $category ),
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fas fa-trash"></i> ', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger',
                                    'title' => 'categorie',
                                    'onclick' => 'return confirm(\'Vraiment supprimer cette categorie ?\')'
                                )) !!}
                                {!! Form::close() !!}
                              </td>
                            </tr>
                          @empty
                          @endforelse
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection
