@extends('admin.layouts.app')
@section('title', 'Categorie')
@section('content')

<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Creation Categorie</h1>
          </div>
          <div class="section-body">
            <div class="row">
              <div class="mx-auto">
                <div class="card-body">
                  @include('admin.categories.form')
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

@endsection
