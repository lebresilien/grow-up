@extends('admin.layouts.app')
@section('title', 'Client')

@section('content')
        <div class="main-content">
          <section class="section">
            <div class="section-header">
              <h1>Edit Admin</h1>
            </div>
            <div class="section-body">
              <div class="row">
                <div class="col-12 col-md-12 col-lg-10">
                  <div class="card profile-widget">
                    <div class="profile-widget-header">                     
                      <div class="profile-widget-items">
                        <div class="profile-widget-item">
                          <div class="profile-widget-item-label">Compte</div>
                          <div class="profile-widget-item-value">{{$client->numero_abonnement}} </div>
                        </div>
                        <div class="profile-widget-item">
                          <div class="profile-widget-item-label">Nom</div>
                          <div class="profile-widget-item-value">{{$client->name}} {{$client->last_name}}</div>
                        </div>
                        <div class="profile-widget-item">
                          <div class="profile-widget-item-label">Date compte</div>
                          <div class="profile-widget-item-value">{{$client->subscription_date}}</div>
                        </div>
                        <div class="profile-widget-item">
                          <div class="profile-widget-item-label">Telephone</div>
                          <div class="profile-widget-item-value">{{$client->telephone}}</div>
                        </div>
                        <div class="profile-widget-item">
                          <div class="profile-widget-item-label">Site</div>
                          <div class="profile-widget-item-value">{{ $client->site_id ? $client->site->name : '' }} </div>
                        </div>
                        <div class="profile-widget-item">
                          <div class="profile-widget-item-label">Solde</div>
                          <div class="profile-widget-item-value">{{$client->solde}}</div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="card-footer text-center">
                      <div class="font-weight-bold mb-2">Follow Ujang On</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
@endsection