<?php
if ($etape->id) {
    $options = ['method' => 'put', 'class'=>'needs-validation',  'url' => action('Admin\EtapeController@update', $etape), 'files' => false];
} else {
    $options = ['method' => 'post', 'class'=>'needs-validation', 'url' => action('Admin\EtapeController@store'), 'files' => false];
}
?>


@include('utilities.errors')
@include('utilities.flash')

{!! Form::model($etape, $options,['class'=>'','enctype'=>"multipart/form-data",'accept-charset'=>"utf-8"]) !!}
@csrf
<div class="card">
    <div class="card-header">
        <h4>Ajouter une etape</h4>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="form-group">
            {!! Form::label('name', 'Nom') !!}
            {!! Form::text('name', null , ['class' => 'form-control', 'required'=>'required', 'placeholder'=>"Name"]) !!}
        </div>
          
        <div class="card">
            <div class="card-footer text-right">
                <button class="btn btn-secondary" type="reset">Reset</button>
                <button class="btn btn-primary" type="submit">Validé</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!} 




