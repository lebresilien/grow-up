@extends('admin.layouts.app')
@section('title', 'Etape')

@section('content')
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Edit Etape</h1>
          </div>
          <div class="section-body">
            <div class="row">
              <div class=" mx-auto">
                <div class="card">
                  <div class="card-header">
                    <h4>Default Tab</h4>
                  </div>
                  <div class="card-body">
                     @include('admin.etapes.form')
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection


