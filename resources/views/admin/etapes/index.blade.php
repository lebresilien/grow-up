@extends('admin.layouts.app')
@section('title', 'Etape')
@section('content')
<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Listing Etape</h1>
          </div>
          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Basic DataTables</h4>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped" id="table-1">
                        <thead>
                          <tr>
                            <th class="text-center">
                              #
                            </th>
                            <th>Name</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @forelse($etapes as $etape)
                            <tr>
                              <td>{{ $loop->iteration }}</td>
                              <td>{{ $etape->name }}</td>
                              
                              <td>
                                <a class="btn btn-primary btn-action mr-1" href="{{ route('etapes.edit', $etape) }}" data-toggle="tooltip"  data-original-title="Edit">
                                  <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a class="btn btn-primary btn-action mr-1" href="{{ route('etapes.show', $etape) }}" data-toggle="tooltip"  data-original-title="Show">
                                  <i class="fas fa-pencil-alt"></i>
                                </a>
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => route('etapes.destroy', $etape ),
                                    'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fas fa-trash"></i> ', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger',
                                    'title' => 'etape',
                                    'onclick' => 'return confirm(\'Vraiment supprimer cette etape ?\')'
                                )) !!}
                                {!! Form::close() !!}
                              </td>
                            </tr>
                          @empty
                          @endforelse
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
@endsection
