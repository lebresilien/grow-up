@extends('admin.layouts.app')
@section('title', 'Devise')
@section('content')

<div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Creation Devise</h1>
          </div>
          <div class="section-body">
            <div class="row">
              <div class="mx-auto">
                <div class="card-body">
                  @include('admin.devises.form')
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>

@endsection
