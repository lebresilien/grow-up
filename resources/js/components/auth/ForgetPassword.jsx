import React, { useState, } from "react";
import { Card, Form, Col, Button, Alert , Nav} from "react-bootstrap";
import {FaLock } from "react-icons/fa"
import TopBar from '../TopBar'


export default function ForgetPassword() {

    const [formSubmitting, setformSubmitting] = useState(false)
    const [isRegistered, setisRegistered] = useState(false)
    const [errorMessage, setErrorMessage] = useState();
    const [error, setError] = useState(false);
    const [email, setEmail] = useState('');

    const handleEmail = (e) => {
         setEmail(e.target.value)
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        setformSubmitting(true)
        let data = { 'email': email}
        axios.post('api/auth/password/create', data).then((response) => {
            console.log(response.data)
            if(response.data.success){
                setError(false)
                setisRegistered(true)
            }else{
               setisRegistered(false)
               setError(true)
               setErrorMessage(response.data.errors)
            }
            setformSubmitting(false)
        })
        .catch(errors => {
            alert(errors)
            //setformSubmitting(false)
        }) 
    }

   
    return (
        <>
           <TopBar />
           <div className="d-flex justify-content-center pt-5">
               <FaLock />
            </div>
            <div className="d-flex justify-content-center pt-5">
               <h1 className="font-weight-bold">Réinitialisation de votre mot de passe.</h1>
            </div>
            <hr className=""/>
            <div className="d-flex justify-content-center pt-5">
              <Card style={{ width: '37rem' }}>
                <Card.Body>
                  <div className="d-flex justify-content-center p-3">
                      {isRegistered ? <Alert variant="success"><h5>Un Mail de Renitialisation vous a été envoyé</h5></Alert> : ''}
                      {error ? <Alert variant="danger">
                           <h5>{errorMessage}</h5>
                      </Alert> : ''}
                   </div>
                   <Form onSubmit={handleSubmit}>
                    <Form.Group>
                     <center><Form.Label>Votre Email: <span style={{color: 'red'}}>*</span></Form.Label></center>
                     <Form.Control type="text" name="email" onChange={handleEmail}>
                     </Form.Control>
                    </Form.Group>
                    <center><Button type="submit" className="p-2">
                       {formSubmitting ? <span className="font-weight-bold ">Envoie...</span> : <span className="font-weight-bold ">Envoyer</span>}
                    </Button></center>
                   </Form >
                </Card.Body>
              </Card>
            </div>
        </>
    )
}