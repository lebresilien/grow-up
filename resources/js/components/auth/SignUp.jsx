import React, { useState, useEffect, useRef } from "react";
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import LinkedIn from "linkedin-login-for-react";
import { Card, Form, Col, Button, Alert } from "react-bootstrap";
import { FaAngleRight } from "react-icons/fa"


export default function SignUp() {
  
  const[country, setCountry] = useState({})
  const [user, setUser] = useState({name: '', email: '', last_name: '', password: '', pays_residence: '', pays_origine: '', identite: '', use_conditions: '',investment: ''});
  const [formSubmitting, setformSubmitting] = useState(false)
  const [isRegistered, setisRegistered] = useState(false)
  const [errorMessage, setErrorMessage] = useState({});
  const [error, setError] = useState('');
  const[userData, setUserData] = useState({})

  useEffect(() => {
    axios.get('api/pays').then(response => {
        setCountry(response.data)
    })
  }, [0])

  const handleChange = e => {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = (e) => {
      e.preventDefault()
      setformSubmitting(true)
      let userData = user;

      axios.post('api/auth/signup', userData).then((response) => {
         if(response.data.success){
          let userData = {
            id: response.data.id,
            name: response.data.name,
            email: response.data.email,
            access_token: response.data.access_token,
            token_type: response.data.token_type
          };
          let appState = {
            isRegistered: true,
            user: userData
          };
          localStorage["appState"] = JSON.stringify(appState);
          setisRegistered(appState.isRegistered)
          setUserData(appState.user)
          setError('')
          location.href = "/"
          
         }else{
            setErrorMessage(response.data.errors)
            setisRegistered(false)
            setError('Whoops! Quelques problèmes avec les données saisies!!')
         }
        setformSubmitting(false)
        
      })
      .catch(error => {
          setError(error)
          setformSubmitting(false)
      })
  }
  
  const responseGoogle = (response) => {
    console.log(response);
  }
  const responseFacebook = (response) => {
    console.log(response);
  }
  const callbackLinkedIn = (error, code, redirectUri) => {
    if (error) {
      // signin failed
    } else {
      // Obtain authorization token from linkedin api
      // see https://developer.linkedin.com/docs/oauth2 for more info
    }
  };

  let errorMessages = errorMessage
  let arr = [];
  Object.values(errorMessage).forEach((value) => (
    arr.push(value)
  ));

  return (
      <>
        <div className="d-flex justify-content-center p-3">
            <h1>Créez votre compte GROW-UP Invest</h1>
        </div>

        <hr className=""/>
        <div className="d-flex justify-content-center pt-3"><b>Connectez-vous avec vos comptes existants</b> : pas de nouveau mot de passe a retenir, connexion en 1 clic</div> 
        <div className="d-flex justify-content-center p-3">
            <div className="P-2">
              <FacebookLogin
                appId="272055243834059"
                autoLoad={true}
                textButton="SE CONNECTER AVEC FACEBOOK"
                fields="name,email,picture"
                callback={responseFacebook} 
              />
            </div>
            <div className="p-2">
            <LinkedIn
              clientId="xxx"
              callback={callbackLinkedIn}
              
              scope={["r_liteprofile","r_emailaddress"]}
              text="SE CONNECTER AVEC LINKEDIN"
            />
            </div>
        </div>

        <div className="d-flex justify-content-center p-1">
           <div className="p-2">
            <GoogleLogin
                clientId="277757494685-it24lhoh4esc227coe27rp2ero8kbq17.apps.googleusercontent.com"
                buttonText="SE CONNECTER AVEC GOOGLE"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
                style={{height: '100px'}}
            />
           </div>
        </div>

        <div className="d-flex justify-content-center">
          <b>Note :</b> GROW-UP ne publiera jamais aucune information sur vos réseaux sans votre autorisation
        </div>

        <div className="d-flex justify-content-center p-3">
            -------------------- ou --------------------
        </div>

        <div className="d-flex justify-content-center p-3">
            {isRegistered ? <Alert variant="success"><h5>Inscription Reussie, un mail vous a été envoyé</h5></Alert> : ''}
            {error ? <Alert variant="danger">
                  <h5>{error}</h5>
                  {arr.map((item, i) => (
                     <p key={i} className="">{item}</p>
                    ))}
                </Alert> : ''}
        </div>

        <div className="d-flex justify-content-center p-3">
            <Card style={{ width: '42rem' }}>
                <Card.Body>
                  <Form onSubmit={handleSubmit}>
                   <Form.Row>   
                    <Form.Group as={Col}>
                     <Form.Label>Identité <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control as="select" onChange={handleChange} name="identite">
                        <option>Monsieur</option>
                        <option>Madame</option>
                     </Form.Control>
                    </Form.Group>
                    <Form.Group as={Col}>
                    
                    </Form.Group>
                   </Form.Row>

                   <Form.Row>
                     <Form.Group as={Col} >
                        <Form.Label>Nom <span style={{color: 'red'}}>*</span></Form.Label>
                        <Form.Control type="text" onChange={handleChange} name="name" />
                     </Form.Group>
                     <Form.Group as={Col} >
                        <Form.Label>Prenom</Form.Label>
                        <Form.Control type="text" onChange={handleChange} name="last_name" />
                     </Form.Group>
                   </Form.Row>
                  
                  <Form.Row>
                     <Form.Group as={Col} controlId="formGridPays">
                        <Form.Label>Pays d'origine <span style={{color: 'red'}}>*</span></Form.Label>
                        <Form.Control as="select" onChange={handleChange} name="pays_origine">
                        {JSON.stringify(country) !== '{}' ? 
                            country.map((countries) =>(
                                <option key={countries.id} value={countries.id}>{countries.name}</option>
                            ))
                            : 
                            ''}
                        </Form.Control>
                     </Form.Group>
                     <Form.Group as={Col} controlId="formGridResidence">
                        <Form.Label>Pays residence <span style={{color: 'red'}}>*</span></Form.Label>
                        <Form.Control as="select" onChange={handleChange} name="pays_residence">
                          {JSON.stringify(country) !== '{}' ? 
                            country.map((countries) =>(
                                <option key={countries.id} value={countries.id}>{countries.name}</option>
                            ))
                            : 
                            ''}
                        </Form.Control>
                     </Form.Group>
                  </Form.Row>

                  <Form.Group>
                     <Form.Label>Email <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="text" onChange={handleChange} name="email"/>
                  </Form.Group>

                  <Form.Group>
                     <Form.Label>Mot de passe <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="password" onChange={handleChange} name="password"/>
                  </Form.Group>

                  <hr className="p-2"/>

                  <Form.Group>
                    <div className="d-flex">
                      <div><Form.Check type="checkbox"/></div>
                        <div>
                          Je souhaite être informé(e) en avant-première des caractéristiques et de l'actualité des projets proposés en financement.
                                    GROW-UP sera votre seul interlocuteur concernant ces opportunités d'investissement et vous garantit que vos informations personnelles ne seront en aucun cas divulguées ou revendues.
                                    Elles ne seront utilisées qu'à des fins de conseil personnalisé. Vous pouvez vous désinscrire à tout moment en modifiant vos paramètres de notifications sur votre compte et à travers des liens de désinscription.
                        </div>
                    </div>
                  </Form.Group>

                  <Form.Group>
                      <div className="d-flex">
                        <div><Form.Check type="checkbox" onChange={handleChange} name="use_conditions" /></div>
                        <div>J'ai lu et j'accepte <a href="#">les Conditions d'Utilisation</a> et la <a href="#">Politique de Confidentialité</a> <span style={{color: 'red'}}>*</span></div>
                      </div>
                  </Form.Group>

                  <Form.Group>
                    <div className="d-flex">
                        <div><Form.Check type="checkbox" onChange={handleChange} name="investment"/></div>
                        <div>
                           En cas d'investissement, j'ai conscience et j’accepte que je peux perdre la totalité de la somme investie (mais jamais plus) et que la revente de mes titres n'est pas garantie <a href="#">(en savoir plus sur les facteurs de risques)</a>
                           <span style={{color: 'red'}}>*</span>
                        </div>
                    </div>
                  </Form.Group>

                  <hr className="p-2"/>

                  <Button type="submit" block >
                      {formSubmitting ? 'CREATION...'  : <span>CREER MON COMPTE <FaAngleRight /></span>  }
                       
                  </Button>
                 </Form>
                </Card.Body>
            </Card>
           
        </div>

        
        
      </>
  )
}

