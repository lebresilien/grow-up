import React, { useState, useEffect, useRef } from "react";
import { Card, Form, Button} from "react-bootstrap";
import {FaLock } from "react-icons/fa"
import TopBar from '../TopBar'


export default function ResetPassword() {

    return (
        <>
           <TopBar />
           <div className="d-flex justify-content-center pt-5">
               <FaLock />
            </div>
            <div className="d-flex justify-content-center pt-5">
               <h1 className="font-weight-bold">Réinitialisation de votre mot de passe.</h1>
            </div>
            <hr className=""/>
            <div className="d-flex justify-content-center pt-5">
              <Card style={{ width: '42rem' }}>
                <Card.Body>
                   <Form >
                    <Form.Group>
                     <center><Form.Label>Nouveau mot de passe <span style={{color: 'red'}}>*</span></Form.Label></center>
                     <Form.Control type="password" name="password">
                     </Form.Control>
                    </Form.Group>
                    <Form.Group>
                     <center><Form.Label>Ancien mot de passe <span style={{color: 'red'}}>*</span></Form.Label></center>
                     <Form.Control type="password" name="confirm-password">
                     </Form.Control>
                    </Form.Group>
                    <center><Button type="submit" className="p-2">
                      <span className="font-weight-bold ">Renitialiser</span>
                    </Button></center>
                   </Form >
                </Card.Body>
              </Card>
            </div>
        </>
    )
}
 if(document.getElementById('reset')){
    ReactDOM.render(<ResetPassword/>, document.getElementById('reset'));
} 
