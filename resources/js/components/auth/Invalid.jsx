import React from "react";
import { Card} from "react-bootstrap";
import TopBar from '../TopBar'


export default function Invalid() {

    return (
        <>
           <TopBar />
            <hr className=""/>
            <div className="d-flex justify-content-center pt-5">
              <Card style={{ width: '42rem' }}>
                <Card.Body>
                   <Card.Title>Token de validation incorrect</Card.Title>
                </Card.Body>
              </Card>
            </div>
        </>
    )
}

if(document.getElementById('invalid')){
  ReactDOM.render(<Invalid/>, document.getElementById('invalid'));
}
