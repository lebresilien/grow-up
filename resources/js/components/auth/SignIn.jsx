import React, { useState, useEffect, useRef } from "react";
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import LinkedIn from "linkedin-login-for-react";
import { Card, Form, Col, Button, Alert , Nav} from "react-bootstrap";
import { FaAngleRight, FaArrowRight } from "react-icons/fa"
import TopBar from '../TopBar'

export default function SignIn() {
   const [user, setUser] = useState({email: '', password: ''})
   const [formSubmitting, setformSubmitting] = useState(false)
   const [isRegistered, setisRegistered] = useState(false)
   const [errorMessage, setErrorMessage] = useState('');
   const [error, setError] = useState(false);
   const[userData, setUserData] = useState({})

    const responseGoogle = (response) => {
        console.log(response);
      }
      const responseFacebook = (response) => {
        console.log(response);
      }
      const callbackLinkedIn = (error, code, redirectUri) => {
        if (error) {
          // signin failed
        } else {
          // Obtain authorization token from linkedin api
          // see https://developer.linkedin.com/docs/oauth2 for more info
        }
    };

   const handleChange = e => {
      setUser({
        ...user,
        [e.target.name]: e.target.value
      });
    };

    const handleSubmit = (e) => {
      e.preventDefault()
      setformSubmitting(true)
      let userData = user;

      axios.post('api/auth/signin', userData).then((response) => {
         
         if(response.data.success){
          let userData = {
            id: response.data.id,
            name: response.data.name,
            email: response.data.email,
            access_token: response.data.access_token,
            token_type: response.data.token_type
          };
          let appState = {
            isLoggedIn: true,
            user: userData
          };
          localStorage["appState"] = JSON.stringify(appState);
          setisRegistered(appState.isLoggedIn)
          setUserData(appState.user)
          setError(false)
          location.href = "/"
          
         }else{
            setErrorMessage(response.data.message)
            setisRegistered(false)
            setError(true)
         }
        setformSubmitting(false)
        
      })
      .catch(errors => {
          alert(errors)
          setformSubmitting(false)
      })
    }

    /* let errorMessages = errorMessage
    let arr = [];
    Object.values(errorMessage).forEach((value) => (
     arr.push(value)
    )); */

    return (
        <>
           <TopBar />
           <hr className=""/>
           <div className="d-flex justify-content-center pt-5">
               <h1 className="font-weight-bold">Connectez-vous à votre compte GROW-UP INVEST</h1>
            </div>
            <div className="d-flex justify-content-center p-3">

            {error ? <Alert variant="danger">
                     <p className="">{errorMessage}</p>
                </Alert> : ''}
            </div>
           {/*  <div className="d-flex justify-content-center">
               <div className="">Pas encore inscrit ?</div>
               <div className="">
                  <Nav>
                  <Nav.Item>
                        <Nav.Link eventKey="1" href="#/home">
                             CREER MON COMPTE <FaArrowRight />
                        </Nav.Link>
                      </Nav.Item>
                  </Nav>
               </div>
            </div> */}
            <hr className="p-3"/>

            <div className="row offset-sm-1 p-5">
               <div className="col-12 col-sm-4">
                  <div className="">
                      <span>Connectez-vous avec vos comptes existants : pas de nouveau</span>
                  </div>
                  <div className="">
                      <span>mot de passe a retenir, pas d'activation par email, connexion</span>
                  </div>
                  <div className="d-flex justify-content-center">
                      <span>en 1 clic...</span>
                  </div>
                  
                  <div className="my-2">
                     <FacebookLogin
                        appId="272055243834059"
                        autoLoad={true}
                        textButton="SE CONNECTER AVEC FACEBOOK"
                        fields="name,email,picture"
                        callback={responseFacebook} 
                      />
                  </div>
                  <div className="my-2">
                     <GoogleLogin
                        clientId="277757494685-it24lhoh4esc227coe27rp2ero8kbq17.apps.googleusercontent.com"
                        buttonText="SE CONNECTER AVEC GOOGLE"
                        onSuccess={responseGoogle}
                        onFailure={responseGoogle}
                        cookiePolicy={'single_host_origin'} 
                      />
                  </div>
                  <div className="my-2">
                     <LinkedIn
                       clientId="xxx"
                       callback={callbackLinkedIn}
                       scope={["r_liteprofile","r_emailaddress"]}
                       text="SE CONNECTER AVEC LINKEDIN"
                      />
                  </div>
                  <div className="d-flex my-2 justify-content-center text-muted ">
                     Note : GROW-UP INVEST ne publiera jamais aucune information sur vos réseaux sans votre autorisation
                  </div>
               </div>
               <div className="col-12 col-sm-2 mt-2 ml-5">
                   <div className="" style={{borderLeft: '1px solid gray', height: '100px'}}>
                       
                    </div>
                    <div className="ml-n1">
                       ou
                    </div>
                    <div className="" style={{borderLeft: '1px solid gray', height: '100px'}}>
                       
                    </div>
               </div>

               <div className="col-12 col-sm-4">
                  {isRegistered ?
                    <div className="d-flex justify-content-center"><Alert variant="success">Redirection...</Alert></div> :
                   ''
                  }
                 
                 <Form onSubmit={handleSubmit}>
                    <Form.Group >
                     <center><Form.Label>Email <span style={{color: 'red'}}>*</span></Form.Label></center>
                     <Form.Control type="text" name="email" onChange={handleChange}>
                     </Form.Control>
                    </Form.Group>
                    <Form.Group>
                    <center><Form.Label>Password <span style={{color: 'red'}}>*</span></Form.Label></center>
                      <Form.Control type="password" name="password" onChange={handleChange}>
                      </Form.Control>
                    </Form.Group>
                    <Button type="submit" block className="p-2">
                       {formSubmitting ? <span className="font-weight-bold ">Connexion...</span> : <span className="font-weight-bold ">Connexion</span>} 
                    </Button>
                 </Form>
                 <div className="d-flex justify-content-center">
                   <Nav variant="pills">
                      <Nav.Item>
                        <Nav.Link eventKey="1" href="/forgot-password">
                             Mot de passe oublié ?
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="2" href="signup">
                             Pas encore inscrit ?
                        </Nav.Link>
                      </Nav.Item>
                   </Nav>
                 </div>
               </div>
               

            </div>
        </>
    )
}  