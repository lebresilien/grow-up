import React from "react";
import './starter.scss'

function Started() {

    const containerStyle = {
        paddingTop: '4em',
        paddingBottom: '5em',
        backgroundColor: 'gray',
        marginTop: '100px'
    };
    const adliteStyle = {
        width: '100%',
        maxWidth: '90rem',
        marginRight: 'auto',
        marginLeft: 'auto',
        paddingRight: '2.4rem',
        paddingLeft: '2.4rem',
    };
    const imageZoneStyle = {
        position: 'relative',
        display: 'flex',
        justifyContent: 'center',
    };
    const floatImgStyle = {
        position: 'absolute',
        top: '-60%',
        justifyContent: 'center',
        width: '350px', 
        height: '380px',
    };
    return (
            <div style={containerStyle}>
                
                    <div  style={adliteStyle}>
                        <div className="row py-1">
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6 " style={imageZoneStyle}>
                                <img src='/images/my-images/woman-in-black.jpg'  className="shadow float-img" alt="" style={floatImgStyle} />
                            </div> 
                            <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                                <h2>Devenez Actionnaire à partir de 10 000 FCFA</h2>
                                <p>
                                    Nous regroupons sur cette plateforme, les meilleurs projets de diverses catégories. Nous vous offrons l'opportunitée, l'environnement technologique les outils nécessaires pour investir dans un secteur concurrenciel.                                </p>
                                <button className="btn btn-primary mt-2">Opportunités d'investissement</button>
                            </div>
                        </div>
                    </div>
                
            </div>
    );
}

export default Started;