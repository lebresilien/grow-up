import React, {useCallback, useState, useMemo} from 'react'
import {useParams} from 'react-router-dom'
import {Container, Card, Form, Button, Col, Modal} from 'react-bootstrap'
import {FaUserEdit, FaLinkedin, FaTwitter, FaFacebook} from 'react-icons/fa'
import {AiOutlineSync} from 'react-icons/ai'
import {useDropzone} from 'react-dropzone'

export default function Presentation(props)
{
    const[showFormUploading, setShowFormUploading] = useState(false)
    const[file, setFile] = useState('')

    const handleShowFormUploading = () => setShowFormUploading(true)
    const handleCloseFormUploading = () => setShowFormUploading(false)

    useEffect(() => {
      setFile(props.presentation)
    }, [props.presentation])

    const baseStyle = {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '20px',
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#eeeeee',
        borderStyle: 'dashed',
        backgroundColor: '#fafafa',
        color: '#bdbdbd',
        outline: 'none',
        transition: 'border .24s ease-in-out',
        height: '350px',
        width: '100%'
      };

      const activeStyle = {
        borderColor: '#2196f3'
      };
      
      const acceptStyle = {
        borderColor: '#00e676'
      };
      
      const rejectStyle = {
        borderColor: '#ff1744'
      };

      const onDrop = useCallback(acceptedFiles => {
        
        let formData = new FormData()
        formData.append('file',  acceptedFiles[0])
        formData.append('id',  props.project)
        formData.append('type',  'presentation')

        const config = {
          headers: { "Content-type": "multipart/form-data", Authorization: `Bearer ${props.user.access_token}`},
          onUploadProgress: progressEvent => setProgressBar( Math.round( (progressEvent.loaded * 100 ) / progressEvent.total) )
       };
       axios.post("/api/v1/projets.upload", formData, config).then((response) => {
         setFile(response.data.presentation)
       })
      }, []);

      const {acceptedFiles, fileRejections, getRootProps,getInputProps,isDragActive,isDragAccept,isDragReject} = useDropzone({accept: 'pdf', multiple: false, onDrop});

      const acceptedFileItems = acceptedFiles.map(file => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
        </li>
      ));

      const fileRejectionItems = fileRejections.map(({ file, errors}) => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
          <ul>
            {errors.map(e => (
              <li key={e.code}>{e.message}</li>
            ))}
          </ul>
        </li>
      ));
      const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
      }), [
        isDragActive,
        isDragReject,
        isDragAccept
      ]);

      


    return (
         <>
          <div className="py-1 mt-3">

            <div className="d-flex justify-content-between">
                <div className="font-weight-bold">Présentation</div>
                <div className="border-upload px-1 pointer" onClick={handleShowFormUploading}><AiOutlineSync /><span className="font-weight-bold pl-1 ">charger une présentation</span></div>
            </div>
           
           {!file ?
            <div>
              <div className="py-3 font-weight-bold align-items-center">
                Votre présentation est un composant essentiel de votre profil. Ajoutez-la à votre profil pour aider les investisseurs à évaluer votre entreprise.
              </div>
             
              <div className="d-flex drag-video-three justify-content-center align-items-center" onClick={handleShowFormUploading}>
                <div className="">Chargez votre présentation au format PowerPoint ou PDF.</div>
              </div>
            </div>
            :
            <div className="">
              
            </div>
           }
          </div>

        
          <Modal show={showFormUploading} onHide={handleCloseFormUploading} size="lg">
             <Modal.Header closeButton></Modal.Header>
             <Modal.Body>
              <div className="container">
                 <div {...getRootProps({style})}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop some files here, or click to select files</p>
                 </div>
                 <aside>
                    <h4>Accepted files</h4>
                    <ul>{acceptedFileItems}</ul>
                    <h4>Rejected files</h4>
                    <ul>{fileRejectionItems}</ul>
                 </aside>
              </div>
             </Modal.Body>
          </Modal>
         </>
    )
}