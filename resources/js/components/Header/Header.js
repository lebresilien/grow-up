import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';

class Header extends Component {

  /*logOut() {

    let appState = {
      isLoggedIn: false,
      user: {}
    };

    localStorage["appState"] = JSON.stringify(appState);
    this.setState(appState);
    this.props.history.push('/login');
  } */
  
  render() {
     const {userData,deconnexion} = this.props;
    return (
       <nav className="navbar navbar-expand-md navbar-light bg-white shadow-sm">
         <div className="container">
           <Link className="navbar-brand" to="/">Laravel</Link>
           <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span className="navbar-toggler-icon">{userData.name}</span>
           </button>
           <div className="collapse navbar-collapse" id="navbarSupportedContent">
               <ul className="navbar-nav mr-auto">

               </ul>
               <ul className="navbar-nav ml-auto">
                  {!this.props.userIsLoggedIn ?
                    <li className="nav-item"><Link to="/login">Login</Link></li>: ""}
                  {this.props.userIsLoggedIn ?
                    <li className="nav-item dropdown">

                      <a id="navbarDropdown" className="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {userData.name}
                            <span className="caret"></span>
                      </a>
                      <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a className="dropdown-item" role="button" onClick={deconnexion}>
                                Deconnexion
                            </a>
                      </div>
                    </li>: ""}
                  </ul>
                </div>
            </div>
      </nav>
    )
  }
}

export default withRouter(Header)