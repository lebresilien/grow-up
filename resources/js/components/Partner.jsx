import React from "react";
import {Container, Row, } from 'react-bootstrap'
import Slider from "react-slick";

function Partner(props)
{
  
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 1000,
        slidesToShow: 3,
        slidesToScroll: 1
    };

    return (
        <>
          <Container fluid className="mt-3">
                        
            <Slider {...settings} > 
                {JSON.stringify(props.partner) !== '{}' ? 
                   props.partner.map((partne) => ( 
                   
                   <span>{partne.name}</span>
                    
                   ))
                  
                  : 
                  ''}
            </Slider>     
              

              
          </Container>
        </>
    )
}

export default Partner