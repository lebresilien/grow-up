import React from "react"
import {useParams, Link} from 'react-router-dom'
import {Navbar, Nav, NavDropdown} from 'react-bootstrap'

function Topbar(props) {

    return (
        <React.Fragment>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                 <Navbar.Brand href="/">Grow-Up</Navbar.Brand>
                 <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                 <Navbar.Collapse id="responsive-navbar-nav">
                   <Nav className="mr-auto">
                      <Nav.Link href="#investir">Investir</Nav.Link>
                      <Nav.Link href="#lever-de-fonds">Faire une levée de fonds</Nav.Link>
                      <Nav.Link href="#contact">Contact</Nav.Link>
                      <Nav.Link href="#blog">Blog</Nav.Link>
                   </Nav>
                   {props.isLoggedIn ? 
                      <NavDropdown title={props.user.name} id="nav-dropdown">
                            <NavDropdown.Item >Dashbord</NavDropdown.Item>
                            <NavDropdown.Item >
                               <Link className="ml-n2" to={`user/${props.user.id}/profile`}>Profile</Link>
                            </NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item  onClick={() => props.deconnexion()}>Deconnexion</NavDropdown.Item>
                      </NavDropdown> : 
                       <Nav>
                         <Nav.Link className="btn btn-primary text-light mr-1 btn-text" href="/signin">Connexion</Nav.Link>
                         <Nav.Link className="btn btn-primary text-light btn-text" href="/signup">Inscription</Nav.Link>
                       </Nav>
                   }
                  </Navbar.Collapse>
            </Navbar>
            
        </React.Fragment>
    )
}

export default Topbar;