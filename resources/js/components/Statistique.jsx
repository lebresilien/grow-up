import React from "react";
import {Card, Container, Row, } from 'react-bootstrap'
import './starter.scss'

function Statistique(props)
{
    var statistiques = [
        {
            name: 'membres',
            nombre: 25
        },
        {
            name: 'en financement',
            nombre: 25
        },
        {
            name: 'financés',
            nombre: 25
        },
        {
            name: 'cloturés',
            nombre: 25
        }
    ]
    return (
        <>
          <Container className="p-5"> 
              <Row>
                <h3 className="offset-4 p-5">Statistique</h3>
              </Row>
              <Row>
                { 
                   statistiques.map((stat) => (
                   <div className="col-md-3 col-6">
                    <Card.Title style={{ marginLeft: '2rem',position: 'relative'}}>{stat.name}</Card.Title>
                    <Card className="bg-dark text-white rounded-circle" style={{ width: '10rem', height:'10rem'}}>
                       <Card.ImgOverlay>
                          <Card.Title className="text-nombre">{stat.nombre}</Card.Title>
                       </Card.ImgOverlay>
                    </Card>
                   </div>
                   ))
                }
              </Row>
          </Container>
          </>
    )

}

export default Statistique