import React, { useState, useRef } from "react"
import { Nav, Navbar, Form, Button, FormControl, InputGroup, OverlayTrigger, Tooltip, ListGroup, Container, Row, Col } from 'react-bootstrap'
import { FaFacebookSquare, FaTwitterSquare, FaYoutubeSquare, FaLinkedin, FaInstagramSquare, FaInfoCircle, FaAngleRight } from "react-icons/fa"
import { IconContext } from "react-icons"

function Footer() {

   function renderTooltip(props) {
      return (
         <Tooltip id="button-tooltip" {...props}>
            Votre email est uniquement utilisé par GROW-UP pour vous communiquer des propositions personnelles d'investissements.
            vous pouvez vous désinscrie à tout moment  en modofiant vos parametres de notifications
         </Tooltip>
      );
   }

   const footerStyle = {
      backgroundColor: '#343a40',
    };
    const borderStyle = {
      borderBottom: '1px dotted',
      color: 'white',
    };
    const borderRightStyle = {
      borderRight: '1px solid white',
    };

   const year = new Date().getFullYear();

   return (
      <React.Fragment>
        <footer className="" style={footerStyle}> 
           <Container>
              <div class="d-flex flex-column flex-sm-row mt-3">
                 <div className="p-2 flex-fill">
                    <div className="d-flex flex-row justify-content-sm-start">
                       <IconContext.Provider value={{ className: "global-class-name", size: "3em" }}>
                          <div className="p-1">
                              <a href="https://www.facebook/grow-up"><FaFacebookSquare /></a>
                          </div>
                          <div className="p-1">
                              <a href="https://www.linkedin/grow-up"><FaLinkedin /></a>
                          </div>
                          <div className="p-1">
                              <a href="https://www.twitter/grow-up"><FaTwitterSquare /></a> 
                          </div>
                          <div className="p-1">
                              <a href="https://www.youtube/grow-up"><FaYoutubeSquare /></a>
                          </div>
                          <div className="p-1">
                              <a href="https://www.instagram/grow-up"><FaInstagramSquare /></a>
                          </div>
                       </IconContext.Provider>
                    </div>
                 </div>
                 <div className="p-2 justify-content-end">
                     <div className="d-flex flex-column">
                        <div className="p-2 flex-fill">
                             <Form.Text className="text-muted">Abonnez-vous à la Newsletter {' '}{' '}
                               <OverlayTrigger placement="top" delay={{ show: 250, hide: 400 }} overlay={renderTooltip}>
                                  <FaInfoCircle />
                                </OverlayTrigger>
                              </Form.Text>
                        </div>
                        <div className="p-2 flex-fill">
                           <Form inline>
                              <InputGroup>
                                <FormControl type="text" placeholder="Search" />
                                <InputGroup.Append>
                                    <Button variant="outline-info">ok</Button>
                                </InputGroup.Append>
                              </InputGroup>
                           </Form>
                        </div>
                     </div>
                 </div>
              </div>
            </Container>
            <Container style={borderStyle} fluid>
              <div className="d-flex  justify-content-md-around flex-column flex-sm-row text-white mb-5">
                 <div className="flex-column">
                      <div className=""><span><b>GROW.UP Invest</b></span> </div>
                      <div className=""><a href="#"><FaAngleRight /> <span className="text-white">Comment ça marche ?</span></a></div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white"> Qui sommes nous ?</span></a> </div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white"> Secteurs d'Investissements</span></a></div>
                      <div className=""><a href="#"><FaAngleRight /> <span className="text-white">Statistiques</span></a></div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white">Actualiés</span></a> </div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white">Partenaires</span></a></div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white">Recrutement</span></a></div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white">Contact</span></a></div>
                 </div>
                 <div className="flex-column">  
                 <div className=""><span><b>Tous les projets</b></span> </div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white">En Campagne</span></a></div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white"> Financés</span></a></div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white"> Cloturés</span></a></div>
                 </div>
                 <div className="flex-column">
                      <div className=""><span><b>Comment lever des fonds ?</b></span> </div>
                      <div className=""><a href="#"><FaAngleRight /><span className="text-white">Déposer un projet</span></a></div>
                 </div>
              </div>
           </Container>

           <Container style={borderStyle} fluid><Container>
                <div className="d-flex text-white my-3 p-5">
                   <div className="flew-fill" style={borderRightStyle}>
                         <span className="text-justify"><b>GROW.UP Invest</b></span><br />
                         <span className="text-justify">Plateforme de financement participartif de nationalité Camerounaise</span>
                   </div>
                   <div className="flex-fill p-5">
                      <span className="text-justify">
                        Spécialiste de l’investissement libéral, GROW.UP Invest propose aux investisseurs de se constituer un portefeuille 
                        diversifié et performant, d'investir dans l’économie réelle et sociale. Nous offrons à des projets porteurs de sens 
                        et créateurs de valeur dans les secteurs de l’environnement, l’immobilier, les énergies renouvelables, le numérique,
                        l'éducation, la santé, l'alimentation, le secondaire et le tertiaire, des solutions de financement rapides, flexibles 
                        et innovantes grâce à des expertises pointues et une forte capacité de collecte.
                      </span><br /><br />
                      <span className="text-justify">
                        <b>Avertissement</b>: Les investissements présentent un risque important de perte partielle ou totale du capital 
                        ainsi qu’un risque d’illiquidité.
                      </span>
                   </div>
                </div>
           </Container></Container>

           <Container  fluid>
              <Container>
                   <div className="d-flex text-white">
                           <div className="p-2 text-black">
                              <span>© 2019-{year} GROW.UP Invest</span>
                           </div>
                           <div className="p-2">
                              <a href="#">Conditions générales d'utilisations</a>
                           </div>
                           <div className="p-2">
                              <a href="#">Mentions légales</a>
                           </div>
                           <div className="p-2">
                              <a href="#">Avertissements sur les risques</a>
                           </div>
                           <div className="p-2">
                              <a href="#">Réclammations</a>
                           </div>
                   </div>
              </Container>
           </Container>
         {/* <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Collapse id="responsive-navbar-nav">
               <Nav className="mr-auto">
                  <IconContext.Provider value={{ className: "global-class-name", size: "3em" }}>
                     <Nav.Link href="https://www.facebook/grow-up"><FaFacebookSquare /></Nav.Link>
                     <Nav.Link href="https://www.linkedin/grow-up"><FaLinkedin /></Nav.Link>
                     <Nav.Link href="https://www.twitter/grow-up"><FaTwitterSquare /></Nav.Link>
                     <Nav.Link href="https://www.youtube/grow-up"><FaYoutubeSquare /></Nav.Link>
                     <Nav.Link href="https://www.instagram/grow-up"><FaInstagramSquare /></Nav.Link>
                  </IconContext.Provider>
               </Nav>
               <Nav>
                  <Form inline>
                     <InputGroup>
                        <Form.Text className="text-muted">Abonnez-vous à la newsletter
                           <OverlayTrigger placement="top" delay={{ show: 250, hide: 400 }} overlay={renderTooltip}>
                              <FaInfoCircle />
                           </OverlayTrigger>
                        </Form.Text>

                        <FormControl type="text" placeholder="Search" />
                        <InputGroup.Append>
                           <Button variant="outline-info">ok</Button>
                        </InputGroup.Append>
                     </InputGroup>
                  </Form>
               </Nav>

            </Navbar.Collapse>

         </Navbar>
         <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Collapse id="responsive-navbar-nav">
               <Nav className="mr-auto">
                  <ListGroup>
                     <ListGroup.Item>
                        <Nav.Link href="#">les projets</Nav.Link>
                     </ListGroup.Item>
                     <ListGroup.Item>
                        <Nav.Link href="https://www.facebook/grow-up"><FaAngleRight /> en Campagne</Nav.Link>
                     </ListGroup.Item>
                     <ListGroup.Item>
                        <Nav.Link href="https://www.facebook/grow-up"><FaAngleRight /> financés</Nav.Link>
                     </ListGroup.Item>
                     <ListGroup.Item>
                        <Nav.Link href="https://www.facebook/grow-up"><FaAngleRight /> cloturés</Nav.Link>
                     </ListGroup.Item>
                  </ListGroup>
               </Nav>
               <Nav>
                  <Form inline>
                     <InputGroup>
                        <Form.Text className="text-muted">Abonnez-vous à la newsletter
                           <OverlayTrigger placement="top" delay={{ show: 250, hide: 400 }} overlay={renderTooltip}>
                              <FaInfoCircle />
                           </OverlayTrigger>
                        </Form.Text>

                        <FormControl type="text" placeholder="Search" />
                        <InputGroup.Append>
                           <Button variant="outline-info">ok</Button>
                        </InputGroup.Append>
                     </InputGroup>
                  </Form>
               </Nav>

            </Navbar.Collapse>

         </Navbar> */}
         </footer> 
      </React.Fragment>
   )
}

export default Footer;