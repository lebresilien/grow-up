import React, {useState} from 'react'

export default function Document(){

    const[plan, setPlan] = useState(false)
    const[prevision, setPrevision] = useState(false)
    const[more, setMore] = useState(false)

    return (
        <>
          <div className="d-flex flex-column mt-5">
             {!plan ? 
                <div className="d-flex flex-column">
                  <div className="d-flex justify-content-between">
                      <div className="p-1 font-weight-bold">Plan d'affaire</div>
                      <div className="p-1 manage-document">Ajouter un document</div>
                  </div>
                  <div className="p-1 text-muted">Quel est votre business plan à long terme ? Types de fichiers préférés : .pdf, .doc, .xls </div>
                </div> 
                 : 
                <div className="d-flex flex-column">
                  <div className="d-flex justify-content-between">
                      <div className="p-1 font-weight-bold">Plan d'affaire</div>
                      <div className="p-1 manage-document">Mettre à jour le document</div>
                  </div>
                  <div className="p-1 text-muted">votre document</div>
                </div>
             }

             {!prevision ? 
                <div className="d-flex flex-column mt-5">
                  <div className="d-flex justify-content-between">
                      <div className="p-1 font-weight-bold">Previsions financières</div>
                      <div className="p-1 manage-document">Ajouter un document</div>
                  </div>
                  <div className="p-1 text-muted">Quelles sont vos prévisions ? Types de fichiers préférés : .pdf, .doc, .xls </div>
                </div> 
                 : 
                <div className="d-flex flex-column mt-5">
                  <div className="d-flex justify-content-between">
                      <div className="p-1 font-weight-bold">Previsions financières</div>
                      <div className="p-1 manage-document">Mettre à jour le document</div>
                  </div>
                  <div className="p-1 text-muted">votre document</div>
                </div>
             }

             
                <div className="d-flex flex-column mt-5">
                  <div className="d-flex justify-content-between">
                      <div className="p-1 font-weight-bold">Documents complementaires</div>
                      <div className="p-1 manage-document">Ajouter un document</div>
                  </div>
                  {!more ?
                   <div className="p-1 text-muted">Ajouter des documents ? Types de fichiers préférés : .pdf, .doc, .xls </div>
                    :
                   <div className="p-1 text-muted">Documents supplementaires</div>
                  }
                  </div> 
                
          </div>
        </>
    )
}