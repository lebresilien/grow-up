import React, {useEffect, useState, useMemo, useCallback } from 'react'
import {useParams} from 'react-router-dom'
import {ProgressBar , Card, Form, Button, Col, Modal} from 'react-bootstrap'
import {FaPlayCircle} from 'react-icons/fa'
import {MdModeEdit, MdAdd} from 'react-icons/md'
import {BsTrash} from 'react-icons/bs'
import {AiOutlineSync} from 'react-icons/ai'
import {useDropzone} from 'react-dropzone'
import ReactPlayer from 'react-player'

export default function Summary(props)
{
     const[showBtnSommary, setShowBtnSommary] = useState(false)
     const[showFormSommary, setShowFormSommary] = useState(false)
     const[showBtnUpload, setShowBtnUpload] = useState(false)
     const[showFormUpload, setShowFormUpload] = useState(false)
     const[showBtnAddUpdate, setShowBtnAddUpdate] = useState(false)
     const[showFormUpdateMember, setShowFormUpdateMember] = useState(false)
     const[showFormAddMember, setShowFormAddMember] = useState(false)
     const[member, setMember] = useState({})
     const[name, setName] = useState('')
     const[position, setPosition] = useState('')
     const[email, setEmail] = useState('')
     const[experience, setExperience] = useState('')
     const[formSubmitting, setFormSubmitting] = useState(false);
     const[errors, setErrors] = useState([])
     const[messageError, setMessageError] = useState('')
     const[concept, setConcept] = useState('')
     const[equipes, setEquipes] = useState([])
     const[progressBar, setProgressBar] = useState(0)
     const[videoFile, setVideoFile] = useState('')
     let temp = ''

     useEffect(() => {
       setConcept(props.concept)
       setEquipes(props.teams)
       setVideoFile(props.video)

     }, [props.teams])
     
     
     const handleShowBtnSommary = () => {
       temp = concept
       setShowBtnSommary(true)
     }
     const handleHideBtnSommary = () => setShowBtnSommary(false);

     const handleShowFormSommary = () => setShowFormSommary(true);
     const handleCloseFormSommary = () => {
       setConcept(temp)
       setShowFormSommary(false);
     }

     const handleShowBtnUpload = () => setShowBtnUpload(true);
     const handleHideBtnUpload = () => setShowBtnUpload(false);

     const handleShowBtnAddUpdate = () => setShowBtnAddUpdate(true);
     const handleHideBtnAddUpdate = () => setShowBtnAddUpdate(false);

     const handleShowFormUpdateMember = () => setShowFormUpdateMember(true);
     const handleCloseFormUpdateMember = () => setShowFormUpdateMember(false);

     const handleShowFormAddMember = () => setShowFormAddMember(true);
     const handleCloseFormAddMember = () => {
       setMessageError('')
       setShowFormAddMember(false);

     }

     const handleChangeSumary = (e) => setConcept(e.target.value)
         
     const uploadVideo = () => setShowFormUpload(true)
     const handleCloseFormUpload = () => setShowFormUpload(false)

    const handleChange = (e) => {
        setMember({...member, [e.target.name] : e.target.value });
    }

    const handleChangeName = (e) => {
      setName(e.target.value)
    }
    const handleChangePosition = (e) => {
      setPosition(e.target.value)
    }
    const handleChangeEmail = (e) => {
      setEmail(e.target.value)
    }
    const handleChangeExperience = (e) => {
      setExperience(e.target.value)
    }

    const handleSubmitMember = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"name": name, "role": position,"email": email,"expertise": experience, "projets_id": props.project }
      
      const config = {
        headers: { Authorization: `Bearer ${props.user.access_token}` }
      };
      axios.post("/api/v1/equipes", data, config).then((response) => {
        setFormSubmitting(false)
        setShowFormAddMember(false)
        setEquipes(equipes => [...equipes, data])
     })
     .catch(errors => {
        setFormSubmitting(false)
        if (errors.response) {
           let err = errors.response.data;
           setErrors(err.errors)
           setMessageError(err.message)
        }
        else if (error.request) {
           let err = errors.request;
           setErrors(err)
        }
        else {
           let err = errors.message;
           setErrors(err)
        }
     }) 
    }

     const baseStyle = {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '20px',
        borderWidth: 2,
        borderRadius: 2,
        borderColor: '#eeeeee',
        borderStyle: 'dashed',
        backgroundColor: '#fafafa',
        color: 'black',
        outline: 'none',
        transition: 'border .24s ease-in-out',
        height: '350px',
        width: '100%'
      };

      const activeStyle = {
        borderColor: '#2196f3'
      };
      
      const acceptStyle = {
        borderColor: '#00e676'
      };
      
      const rejectStyle = {
        borderColor: '#ff1744'
      };

      const onDrop = useCallback(acceptedFiles => {
        
        let formData = new FormData()
        formData.append('file',  acceptedFiles[0])
        formData.append('id',  props.project)
        const config = {
          headers: { "Content-type": "multipart/form-data", Authorization: `Bearer ${props.user.access_token}`},
          onUploadProgress: progressEvent => setProgressBar( Math.round( (progressEvent.loaded * 100 ) / progressEvent.total) )
       };
       axios.post("/api/v1/projets.video", formData, config).then((response) => {
         setVideoFile(response.data.video)
         console.log((response.data.video))
       })
      }, []);

      const {acceptedFiles, fileRejections, getRootProps,getInputProps,isDragActive,isDragAccept,isDragReject} = useDropzone({accept: 'video/*', multiple: false, onDrop});

      const acceptedFileItems = acceptedFiles.map(file => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
        </li>
      ));

      

      const fileRejectionItems = fileRejections.map(({ file, errors}) => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
          <ul>
            {errors.map(e => (
              <li key={e.code}>{e.message}</li>
            ))}
          </ul>
        </li>
      ));
      const style = useMemo(() => ({
        ...baseStyle,
        ...(isDragActive ? activeStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
      }), [
        isDragActive,
        isDragReject,
        isDragAccept
      ]);

      function removeMember(e,index) {
        let filteredTeam = equipes.filter((item,itemIndex) => index !== itemIndex)
        let data = {"expertise": filteredTeam}

        const config = {
          headers: { Authorization: `Bearer ${props.user.access_token}` }
        };

        axios.put("/api/v1/projets/"+props.project, data, config).then((response) => {
          setEquipes(filteredTeam)
       })
       .catch(errors => {
         alert(errors)
       })
        
      }

      function handleChangeNames(index, e) {
        const updatedEquipe = [...equipes]
        updatedEquipe[index] = {...updatedEquipe[index], name: e.target.value}
        setEquipes(updatedEquipe) 
      }

      function handleChangePositions(index, e) {
        const updatedEquipe = [...equipes]
        updatedEquipe[index] = {...updatedEquipe[index], role: e.target.value}
        setEquipes(updatedEquipe) 
      }

      function handleChangeExpertises(index, e) {
        const updatedEquipe = [...equipes]
        updatedEquipe[index] = {...updatedEquipe[index], expertise: e.target.value}
        setEquipes(updatedEquipe) 
      }


      const submitFormSummary = (e) => {
         e.preventDefault()
         setFormSubmitting(true)
         const config = {
          headers: { Authorization: `Bearer ${props.user.access_token}` }
         };
         let data = {"concept": concept, "projets_id": props.project }
         axios.put("/api/v1/projets/"+props.project, data, config).then((response) => {
          setShowFormSommary(false)
       })
       .catch(errors => {
         alert(errors)
       })
      }

     return(
         <>
           <div className="d-flex flex-column my-3">
              <div className="py-2" onMouseEnter={handleShowBtnSommary} onMouseLeave={handleHideBtnSommary}>
                  <div className="d-flex justify-content-between">
                     <div className="font-weight-bold">Company Sommary</div>
                     {showBtnSommary ? <div className=""><MdModeEdit className="pointer" size={25}  onClick={handleShowFormSommary}/></div> : '' }
                  </div>
              </div> 
              <div className="py-2"> {props.concept}</div>
            
              {!videoFile ? 
              <div className="p-0 m-0 pointer" onMouseEnter={handleShowBtnUpload} onMouseLeave={handleHideBtnUpload} onClick={uploadVideo}>
                  <div className="d-flex drag-video-one justify-content-center align-items-end">
                        <div className="">increase the impact of your profile by uploading a short spicth video</div>
                  </div>
                  <div className="d-flex drag-video-second align-items-end justify-content-end">
                     {showBtnUpload ? <div className="p-2 border-upload m-2"><AiOutlineSync size={25}/><span className="">Charger La Video</span></div> : ''}
                  </div>
              </div>
              :
              <div className="p-0 m-0 pointer" onMouseEnter={handleShowBtnUpload} onMouseLeave={handleHideBtnUpload}>
                   
                        <ReactPlayer url={videoFile} playIcon={<FaPlayCircle />} controls={true} width="100%" height="100%" />
                      
                      <div className="d-flex align-items-end justify-content-end">
                        {showBtnUpload ? <div className="p-2 border-upload m-2" onClick={uploadVideo}><AiOutlineSync size={25}/><span className="">Charger La Video</span></div> : ''}
                     </div>
              </div>

              }


              <div className="my-4" onMouseEnter={handleShowBtnAddUpdate} onMouseLeave={handleHideBtnAddUpdate}>
                 <div className="d-flex justify-content-between">
                    <div className="font-weight-bold p-1">Equipe</div>
                    {showBtnAddUpdate ? <div className="d-flex ">
                        <div className="mx-0 d-flex justify-content-center align-items-center add-update-update"><MdModeEdit size={25} onClick={handleShowFormUpdateMember} /></div>
                        <div className="mx-0 d-flex justify-content-center align-items-center add-update-add"><MdAdd size={25} onClick={handleShowFormAddMember} />
                    </div></div>  : '' }
                 </div>
                 <div className="d-flex my-3" style={{borderTop: '1px solid #eee'}}></div>

                 {equipes.length !== 0 ? 
                   equipes.map((team,index) => (
                    <div className="py-3 row mx-1" key={index} style={{borderBottom: '1px solid #eee'}}>
                      <div className="col-sm-2 col-md-1 col-2">
                        <img src="/images/defaults/avatar.png" height={50} width={50} className="rounded-circle" />
                      </div>
                      <div className="col-sm-10 col-md-3 col-10 left-control">
                        <div className="d-flex flex-column">
                          <div className="text-wrap font-weight-bold d-flex flex-fill" >{team.name}</div>
                          <div className="text-muted text-uppercase">{team.role}</div>
                        </div>
                      </div>
                      <div className="col-sm-12 col-12 col-md-6 offset-md-1">{team.expertise}</div>
                    </div>
                   ))
                  :
                 ''}

              </div>
           </div>

           <Modal show={showFormSommary} onHide={handleCloseFormSommary}>
            <Modal.Header closeButton>
              <Modal.Title>Sommaire de votre entreprise</Modal.Title>
            </Modal.Header>
            <Form onSubmit={submitFormSummary}>
            <Modal.Body>
                  <Form.Group>
                    <Form.Label>Somaire</Form.Label>
                     <Form.Control  as="textarea"  rows={5} name="summary" value={concept} onChange={handleChangeSumary}/>
                  </Form.Group>  
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleCloseFormSommary}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''} >
                {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormUpload} onHide={handleCloseFormUpload} size="lg">
            <Modal.Header closeButton></Modal.Header>
            <Modal.Body>
              <div className="container">
                 <div {...getRootProps({style})}>
                    <input {...getInputProps()} />
                    <p>Drag 'n' drop some files here, or click to select files</p>
                 </div>
                 <aside>
                    <h4>Accepted files</h4>
                    <ul>{acceptedFileItems}</ul>
                    <h4>Rejected files</h4>
                    <ul>{fileRejectionItems}</ul>
                    <ProgressBar animated now={progressBar} />
                 </aside>
              </div>
            </Modal.Body>
          </Modal>

          <Modal show={showFormAddMember} onHide={handleCloseFormAddMember}>
            <Modal.Header closeButton>Ajouter un membre</Modal.Header>
            <Form onSubmit={handleSubmitMember}>
            <Modal.Body>
            <div className="d-flex flex-column">
                     {messageError ? <div className="p-2 justify-content-center"><h5 className="alert alert-danger">{messageError}</h5></div> : ''}
      
                     {messageError ?
                     <div className="p-2 justify-content-center">
                        <ul className="list-group">
                           {errors.name ? <li className="list-group-item alert alert-danger">{errors.name}</li> : '' }
                           {errors.role ? <li className="list-group-item alert alert-danger">{errors.role}</li> : '' }
                           {errors.email ? <li className="list-group-item alert alert-danger">{errors.email}</li> : '' }
                        </ul>
                     </div>
                     :
                     ''
                     }

                     
                  </div>
                
                  <Form.Group>
                    <Form.Label>Nom <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="text" onChange={handleChangeName} />
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Position</Form.Label>
                     <Form.Control  type="text"  name="position" onChange={handleChangePosition} />
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Experience et Expertise</Form.Label>
                     <Form.Control  as="textarea"  name="experience" onChange={handleChangeExperience} rows={3}/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Email <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="email"  name="email" onChange={handleChangeEmail} />
                  </Form.Group>    
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleCloseFormAddMember}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
                {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormUpdateMember} onHide={handleCloseFormUpdateMember}>
            <Modal.Header closeButton>Modifier vos membre</Modal.Header>
            <Form>
            <Modal.Body>
                {equipes.length !== 0 ? 
                 equipes.map((team,index) => (
                  <div className="py-3" key={index}>
                  <div className="card">
                  <div className="mx-3">
                  <Form.Group>
                    <Form.Label>Nom</Form.Label>
                     <Form.Control  type="text" name="name" value={team.name} onChange={() => handleChangeNames(index,event)} />
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Position</Form.Label>
                     <Form.Control  type="text"  name="role" value={team.role} onChange={() => handleChangePositions(index,event)}/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Experience et Expertise</Form.Label>
                     <Form.Control  as="textarea"  name="experience" rows={3} value={team.expertise} onChange={() => handleChangeExpertises(index,event)}/>
                  </Form.Group>
                  <Form.Group>
                     <Form.Control  type="hidden"  name="email" value={team.email} />
                  </Form.Group>
                  <div className="d-flex py-1">
                    <Button className="mb-3" variant="outline-primary" onClick={() => removeMember(event, index)}>Supprimer le membre de l'equipe</Button>
                  </div>
                  </div>
                  </div>
                  </div>
                 ))
                 : 
                 ''}    
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleCloseFormUpdateMember}>Annuler</Button>
             <Button variant="primary" >Enregistrer</Button>
            </Modal.Footer>
            </Form>
          </Modal>
         </>
     )
}
