import React, {useEffect, useState, useMemo } from 'react'
import {Container, Card, Form, Button, Col, Modal} from 'react-bootstrap'
import {MdModeEdit, MdAdd} from 'react-icons/md'

export default function Information(props)
{
    const[formSubmitting, setFormSubmitting] = useState(false)
    const[showBtnTeam, setShowBtnTeam] = useState(false)
    const[showBtnProb, setShowBtnProb] = useState(false)
    const[showBtnProduit, setShowBtnProduit] = useState(false)
    const[showBtnCible, setShowBtnCible] = useState(false)
    const[showBtnModel, setShowBtnModel] = useState(false)
    const[showBtnClient, setShowBtnClient] = useState(false)
    const[showBtnStrategy, setShowBtnStrategy] = useState(false)
    const[showBtnConcu, setShowBtnConcu] = useState(false)
    const[showBtnAvantage, setShowBtnAvantage] = useState(false)
    const[showFormTeam, setShowFormTeam] = useState(false)
    const[showFormProb, setShowFormProb] = useState(false)
    const[showFormProduit, setShowFormProduit] = useState(false)
    const[showFormCible, setShowFormCible] = useState(false)
    const[showFormModel, setShowFormModel] = useState(false)
    const[showFormClient, setShowFormClient] = useState(false)
    const[showFormStrategy, setShowFormStrategy] = useState(false)
    const[showFormConcu, setShowFormConcu] = useState(false)
    const[showFormAvantage, setShowFormAvantage] = useState(false)
    const[team, setTeam] = useState('')
    const[prob, setProb] = useState('')
    const[produit, setProduit] = useState('')
    const[cibles, setCibles] = useState('')
    const[models, setModels] = useState('')
    const[clients, setClients] = useState('')
    const[strategies, setStrategies] = useState('')
    const[concu, setConcu] = useState('')
    const[avantages, setAvantages] = useState('')
    const[prev, setPrev] = useState('') 
    const config = {headers: { Authorization: `Bearer ${props.user.access_token}` } };
     useEffect(() => {
      setModels(props.model)
      setProb(props.problematique)
      setProduit(props.service)
      setCibles(props.cible)
      setClients(props.client)
      setAvantages(props.avantage)
      setConcu(props.concurrent)
      setStrategies(props.model)
      setTeam(props.equipe)
    }, [props.avantage]) 

    
    const handleShowBtnTeam = () => setShowBtnTeam(true);
    const handleHideBtnTeam = () => setShowBtnTeam(false);
    const handleShowFormTeam  = () => {
      setPrev(team)
      setShowFormTeam(true) 
   }
   const handleHideFormTeam  = () => {
      setTeam(prev)
      setShowFormTeam(false);
   }
    const handleSubmitTeam = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"equipe_direction": team}
      setTimeout(() => {
        axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
        setShowFormTeam(false)
        setFormSubmitting(false)
        })
      },
      2000)   
   }

    const handleShowBtnProb = () => setShowBtnProb(true);
    const handleHideBtnProb = () => setShowBtnProb(false);
    const handleShowFormProb  = () => {
      setPrev(prob)
      setShowFormProb(true);  
   }
   const handleHideFormProb  = () => {
      setProb(prev)
      setShowFormProb(false);
   }
    const handleSubmitProb = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"problematique": prob}
      setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormProb(false)
            setFormSubmitting(false)
         })
      },
      2000)   
   }

    const handleShowBtnProduit = () => setShowBtnProduit(true);
    const handleHideBtnProduit = () => setShowBtnProduit(false);
    const handleShowFormProduit  = () => {
      setPrev(produit)
      setShowFormProduit(true);  
   }
   const handleHideFormProduit  = () => {
      setProduit(prev)
      setShowFormProduit(false);
   }
   const handleSubmitProduit = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"product_service": produit}
      setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormProduit(false)
            setFormSubmitting(false)
         })
      },
      2000)   
   }

    const handleShowBtnCible = () => setShowBtnCible(true);
    const handleHideBtnCible = () => setShowBtnCible(false);
    const handleShowFormCible  = () => {
      setPrev(cibles)
      setShowFormCible(true)  
   }
   const handleHideFormCible  = () => {
      setCibles(prev)
      setShowFormCible(false)
   }
    const handleSubmitCible = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"market_target": cibles}
      setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormCible(false)
            setFormSubmitting(false)
         })
      },
      2000)   
   }

   const handleShowBtnModel = () => setShowBtnModel(true);
   const handleHideBtnModel = () => setShowBtnModel(false);

   const handleShowFormModel = () => {
      setPrev(models)
      setShowFormModel(true);  
   }
   const handleHideFormModel = () => {
      setModels(prev)
      setShowFormModel(false);
   }
    const handleSubmitModel = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"business_model": models}
      setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormModel(false)
            setFormSubmitting(false)
         })
      },
      2000)   
   }

    const handleShowBtnClient = () => setShowBtnClient(true);
    const handleHideBtnClient = () => setShowBtnClient(false);
    const handleShowFormClient  = () => {
      setPrev(clients)
      setShowFormClient(true) 
   }
   const handleHideFormClient = () => {
      setClients(prev)
      setShowFormClient(false)
   }
    const handleSubmitClient = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"clients": clients}
      setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormClient(false)
            setFormSubmitting(false)
         })
      },
      2000)   
   }

    const handleShowBtnStrategy = () => setShowBtnStrategy(true);
    const handleHideBtnStrategy = () => setShowBtnStrategy(false);
    const handleShowFormStrategy  = () => {
      setPrev(strategies)
      setShowFormStrategy(true);  
   }
   const handleHideFormStrategy  = () => {
      setStrategies(prev)
      setShowFormStrategy(false);
   }
    const handleSubmitStrategy = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"sale_marketing_strategy": strategies}
      setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormStrategy(false)
            setFormSubmitting(false)
         })
      },
      2000)   
   }

    const handleShowBtnConcu = () => setShowBtnConcu(true);
    const handleHideBtnConcu  = () => setShowBtnConcu (false);
    const handleShowFormConcu  = () => {
      setPrev(concu)
      setShowFormConcu(true);  
   }
   const handleHideFormConcu  = () => {
      setConcu(prev)
      setShowFormConcu(false);
   }
    const handleSubmitConcu = (e) => {
      e.preventDefault()
      setFormSubmitting(true)
      let data = {"concurrent": concu}
      setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormConcu(false)
            setFormSubmitting(false)
         })
      },
      2000)   
   }

    const handleShowBtnAvantage = () => setShowBtnAvantage(true);
    const handleHideBtnAvantage = () => setShowBtnAvantage(false);
    const handleShowFormAvantage  = () => {
       setPrev(avantages)
       setShowFormAvantage(true);  
    }
    const handleHideFormAvantage  = () => {
        setAvantages(prev)
        setShowFormAvantage(false);
    }
    const handleSubmitAvantage = (e) => {
       e.preventDefault()
       setFormSubmitting(true)
       let data = {"avantage_concurrentiel": avantages}
       setTimeout(() => {
         axios.put("/api/v1/projets/"+props.projectID, data, config).then((response) => {
            setShowFormAvantage(false)
            setFormSubmitting(false)
         })
       },
       2000)   
    }

    const handleChangeTeam = (e) => setTeam(e.target.value)
    const handleChangeProb = (e) => setProb(e.target.value)
    const handleChangeProduit = (e) => setProduit(e.target.value)
    const handleChangeCible = (e) => setCibles(e.target.value)
    const handleChangeModel = (e) => setModels(e.target.value)
    const handleChangeClient = (e) => setClients(e.target.value)
    const handleChangeStrategy = (e) => setStrategies(e.target.value)
    const handleChangeConcu = (e) => setConcu(e.target.value)
    const handleChangeAvantage = (e) => setAvantages(e.target.value)
   

    return (
        <>

          <div className="d-flex flex-column mt-5">
            {team == null ? 
             <div className="p-3 flex-column column-entreprise"  onMouseEnter={handleShowBtnTeam} onMouseLeave={handleHideBtnTeam}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Equipe de direction</div>
                    <div>
                       {showBtnTeam ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormTeam}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                 Quel est l'avantage concurrentiel de votre entreprise (par exemple procédés, brevets, avantage de précurseur, expertise ou technologie exclusive) ?
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100"  onMouseEnter={handleShowBtnTeam} onMouseLeave={handleHideBtnTeam}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Equipe de direction</div>
                    <div>
                       {showBtnTeam ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormTeam}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{team}</div>
             </div>
             
            }

            {prob == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnProb} onMouseLeave={handleHideBtnProb}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Problématique client</div>
                    <div>
                       {showBtnProb ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormProb}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                  Quelle problématique client votre produit ou service permet-il de résoudre ?
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnProb} onMouseLeave={handleHideBtnProb}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Problématique client</div>
                    <div>
                       {showBtnProb ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormProb}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{prob}</div>
             </div>
             
            }    


            {produit == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnProduit} onMouseLeave={handleHideBtnProduit}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Produits et Services</div>
                    <div>
                       {showBtnProduit ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormProduit}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                  Décrivez votre produit et/ou service et précisez les avantages clés pour vos clients.
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnProduit} onMouseLeave={handleHideBtnProduit}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Produits et Services</div>
                    <div>
                       {showBtnProduit ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormProduit}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{produit}</div>
             </div>
             
            }  

             {cibles == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnCible} onMouseLeave={handleHideBtnCible}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Marché cible</div>
                    <div>
                       {showBtnCible ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormCible}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                  Indiquez les caractéristiques géographiques, démographiques et psychographiques de votre marché cible.
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnCible} onMouseLeave={handleHideBtnCible}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Marché cible</div>
                    <div>
                       {showBtnCible ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormCible}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{cibles}</div>
             </div>
             
            }   

            {models == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnModel} onMouseLeave={handleHideBtnModel}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Business model</div>
                    <div>
                       {showBtnModel ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormModel}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                 Quelle stratégie allez-vous mettre en œuvre pour accroître et préserver la valeur de l'entreprise ?
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnModel} onMouseLeave={handleHideBtnModel}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Business model</div>
                    <div>
                       {showBtnModel ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormModel}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{models}</div>
             </div>
             
            }

            {clients == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnClient} onMouseLeave={handleHideBtnClient}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Clients</div>
                    <div>
                       {showBtnClient ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormClient}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                  Expliquez qui sont vos clients et les raisons pour lesquelles vous les ciblez plus particulièrement.
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnClient} onMouseLeave={handleHideBtnClient}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Clients</div>
                    <div>
                       {showBtnClient ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormClient}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{clients}</div>
             </div>
             
            }

            {strategies == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnStrategy} onMouseLeave={handleHideBtnStrategy}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Stratégie Marketing et Ventes</div>
                    <div>
                       {showBtnStrategy ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormStrategy}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                     Quelle stratégie appliquez-vous pour acquérir et fidéliser vos clients ? Comment allez-vous promouvoir et commercialiser votre offre et fidéliser vos clients ?
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnStrategy} onMouseLeave={handleHideBtnStrategy}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Stratégie Marketing et Ventes</div>
                    <div>
                       {showBtnStrategy ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormStrategy}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{strategies}</div>
             </div>
             
            }

            {concu == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnConcu} onMouseLeave={handleHideBtnConcu}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Concurrents</div>
                    <div>
                       {showBtnConcu ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormConcu}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                    Décrivez l'environnement concurrentiel et indiquez les points forts et les points faibles de vos concurrents.?
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnConcu} onMouseLeave={handleHideBtnConcu}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Concurrents</div>
                    <div>
                       {showBtnConcu ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormConcu}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{concu}</div>
             </div>
             
            }

            
           {avantages == null ? 
             <div className="p-3 flex-column column-entreprise mt-3"  onMouseEnter={handleShowBtnAvantage} onMouseLeave={handleHideBtnAvantage}>
               <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Avantages concurrentiels</div>
                    <div>
                       {showBtnAvantage ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormAvantage}><MdModeEdit size={25} /></div> : '' }
                   </div>
               </div>
               <div className="mt-3">
                  Quel est l'avantage concurrentiel de votre entreprise (par exemple procédés, brevets, avantage de précurseur, expertise ou technologie exclusive) ?nts.?
               </div>
             </div>

             :

             <div className="p-3 flex-column column-entreprise-data w-100 mt-3"  onMouseEnter={handleShowBtnAvantage} onMouseLeave={handleHideBtnAvantage}>
                <div className="d-flex justify-content-between">
                   <div className="font-weight-bold py-2">Avantages concurrentiels</div>
                    <div>
                       {showBtnAvantage ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowFormAvantage}><MdModeEdit size={25} /></div> : '' }
                   </div>
                </div>
                <div className="mt-3">{avantages}</div>
             </div>
             
            }

          </div>

           <Modal show={showFormTeam} onHide={handleHideFormTeam}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Equipe de votre entreprise</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitTeam}>
            <Modal.Body>
               <Form.Group>
                  <Form.Control  as="textarea"  rows={5} name="team" value={team} onChange={handleChangeTeam}/>
               </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleSubmitTeam}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
               {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>


          <Modal show={showFormProb} onHide={handleHideFormProb}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Problématique client</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitProb}>
            <Modal.Body>
                  <Form.Group>
                     <Form.Control  as="textarea"  rows={5} name="prob" value={prob} onChange={handleChangeProb}/>
                  </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormProb}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
                {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormProduit} onHide={handleHideFormProduit}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Produits et Services</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitProduit}>
            <Modal.Body>
               <Form.Group>
                  <Form.Control  as="textarea"  rows={5} name="produit" value={produit} onChange={handleChangeProduit}/>
               </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormProduit}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
                {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormCible} onHide={handleHideFormCible}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Marché cible</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitCible}>
            <Modal.Body>
                  <Form.Group>
                     <Form.Control  as="textarea"  rows={5} name="cible" value={cibles} onChange={handleChangeCible}/>
                  </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormCible}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
               {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormModel} onHide={handleHideFormModel}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Business model </Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitModel}>
            <Modal.Body>
                  <Form.Group>
                     <Form.Control  as="textarea"  rows={5} name="model" value={models} onChange={handleChangeModel}/>
                  </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormModel}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''} >
                 {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormClient} onHide={handleHideFormClient}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Clients </Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitClient}>
            <Modal.Body>
               <Form.Group>
                  <Form.Control  as="textarea"  rows={5} name="client" value={clients} onChange={handleChangeClient}/>
               </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormClient}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
                {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormStrategy} onHide={handleHideFormStrategy}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Stratégie Marketing et Ventes </Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitStrategy}>
            <Modal.Body>
               <Form.Group>
                  <Form.Control  as="textarea"  rows={5} name="strategy" value={strategies} onChange={handleChangeStrategy}/>
               </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormStrategy}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
               {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormConcu} onHide={handleHideFormConcu}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Concurrents </Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitConcu}>
            <Modal.Body>
               <Form.Group>
                  <Form.Control  as="textarea"  rows={5} name="concu" value={concu} onChange={handleChangeConcu}/>
               </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormConcu}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
               {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormAvantage} onHide={handleHideFormAvantage}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Avantages concurrentiels </Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitAvantage}>
            <Modal.Body>
               <Form.Group>
                   <Form.Control  as="textarea"  rows={5} name="avantage" value={avantages} onChange={handleChangeAvantage} />
               </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideFormAvantage}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
               {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

        </>
    )
}