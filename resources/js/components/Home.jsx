import React, {Component, useEffect, useState} from 'react'
import {Container, Card} from 'react-bootstrap'
import Slide from './Slide'
import TopBar from './TopBar'
import Footer from './Footer'
import Started from './Started'
import Categories from './Categories'
import Statistique from './Statistique'
import Partner from './Partner'

function Home() {
  
    const[topic, setTopic] = useState({});
    const[partners, setPartners] = useState({});
    const[loading, setLoading] = useState(false);
    const[isLoggedIn, setisLoggedIn] = useState(false);
    const[user, setUser] = useState({});

    const getCategories = async () => {
        let res = await axios.get("api/category");
        let data  = await res.data;
        setTopic(data)
        setLoading(true)
        
    };

    const getPartners = async () => {
      let res = await axios.get("api/partner");
      let data  = await res.data;
      setPartners(data)
    };

    const deconnexion = () => {
      let appState = {
        isLoggedIn: false,
        user: {}
      };
      localStorage["appState"] = JSON.stringify(appState);
      setisLoggedIn(JSON.parse(localStorage['appState']).isLoggedIn)
    }
       
    useEffect(() => {
      getCategories()
      getPartners()
      setisLoggedIn(JSON.parse(localStorage['appState']).isLoggedIn)
      setUser(JSON.parse(localStorage['appState']).user)
    },[loading])
  
    return (
      <div className="">
        <TopBar isLoggedIn={isLoggedIn} user={user} deconnexion={deconnexion}/>
        <Slide />
        <Started />
        <Categories category={topic}/>
        
        <Container>

        <div className="row offset-4 p-5">
           <h3 className="text-center text-uppercase">Produits et services offerts</h3>
        </div>
       
        <div className="row">
          <div className="col-11">
          <div className="row">
           <div className="col-12 col-md-4">
              <Card style={{ width: '18rem', height:'20rem'}}>
                  <Card.Img variant="top" src="images/my-images/capital.png" />
                  <Card.Body>
                      <Card.Text>Investir en capital (Actions et Obligations financières) dans des entreprises permet de devenir Membre (Actionnaire ou Obligataire) </Card.Text>
                  </Card.Body>
              </Card>
           </div>
           <div className="col-12 col-md-4">
              <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="images/my-images/don.jpg" />
                  <Card.Body>
                      <Card.Text>Investir en donation dans des projets sociaux pour contribuer au bien etre d'autrui </Card.Text>
                  </Card.Body>
              </Card>
           </div>
           <div className="col-12 col-md-4">
              <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="images/my-images/conseil.png" />
                  <Card.Body>
                      <Card.Text>GROW.UP Invest accompagne les entrepreneurs dans leurs perspectives à travers un service compétitif d'appui - conseil en affaires et business development </Card.Text>
                  </Card.Body>
              </Card>
           </div>
           </div></div>
        </div>
        </Container>
        <Statistique />
        <Partner partner={partners}/>
        <Footer/>
      </div> 
      )
    
  }

export default Home