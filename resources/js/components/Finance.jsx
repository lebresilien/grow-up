import React, {useEffect, useState, useMemo } from 'react'
import {useParams} from 'react-router-dom'
import {Container, Table, Form, Button, Col, Modal} from 'react-bootstrap'
import {MdModeEdit, MdAdd} from 'react-icons/md'
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import { registerLocale, setDefaultLocale } from  "react-datepicker";
import es from 'date-fns/locale/fr';

export default function Finance(props)
{
    const[showBtn, setShowBtn] = useState(false)
    const[showForm, setShowForm] = useState(false)
    const[formSubmitting, setFormSubmitting] = useState(false)
    const[startDate,setStartDate] = useState(new Date())
    const[showTable, setShowTable] = useState(false)
    const[previous, setPrevious] = useState('')
    let prevValue = ''
    registerLocale('fr', es)

    const handleShowBtn = () => setShowBtn(true)
    const handleHideBtn = () => setShowBtn(false)

    const handleShowForm  = () => {
        //prev = avantage
        //setAvan(prevAvantage)
        setShowForm(true);  
     }
     const handleHideForm  = () => {
         //setAvantage(avan)
         setShowForm(false);
     }
     const handleSubmit = (e) => {
        e.preventDefault()
        setFormSubmitting(true)
        setTimeout(() => {
          setFormSubmitting(false)
          setShowForm(false);
          setShowTable(true);
        },
        2000)   
     }

     const handleChangeDate = date => setStartDate(date) 


    return(
        <>
          <div className="mt-5">
             
            
              <div className="d-flex flex-column" onMouseEnter={handleShowBtn} onMouseLeave={handleHideBtn}>
                  <div className="d-flex justify-content-between">
                       <div className="font-weight-bold py-2">Tour de financement actuel</div>
                       <div className="">
                          {showBtn ? <div className="border-upload p-1 d-flex justify-content-center align-items-center pointer" onClick={handleShowForm}><MdModeEdit size={25} /></div> : '' }
                       </div>
                  </div>
                  {!showTable ?
                    <div className="mt-3 p-3 column-entreprise">
                      Quel est le montant que vous souhaitez récolter, definissez le prix d'une action et la durée du financement
                    </div>
                  :
                  <div>
                  <Table responsive>
                   <tbody>
                    <tr>
                        <td>Recherche</td><td colSpan="2"></td><td></td><td className="d-flex justify-content-end">52555</td> 
                    </tr>
                    <tr>
                        <td>Montant Action</td><td colSpan="2"></td><td></td><td className="d-flex justify-content-end">52555</td>
                    </tr>
                    <tr>
                        <td>Fin de la lévée</td><td colSpan="2"></td><td></td><td className="d-flex justify-content-end">hchchchc</td>
                    </tr>
                   </tbody>
                 </Table>
                 <div className="d-flex justify-content-end">
                    <Button variant="primary">
                        CLOTURER LA LEVEE
                    </Button>
                 </div>
                 </div>
                 }
              </div>
                           
          </div>

          <Modal show={showForm} onHide={handleHideForm}>
            <Modal.Header closeButton>
              <Modal.Title className="font-weight-bold">Modifier votre tour de financement </Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmit}>
            <Modal.Body>
               <Form.Group>
                   <Form.Label>Montant de la lévée</Form.Label>
                   <Form.Control  type="text" name="amount" />
               </Form.Group>
               <Form.Group>
                   <Form.Label>Prix de l'action</Form.Label>
                   <Form.Control  type="text" name="price" />
               </Form.Group>
               <Form.Group>
                   <Form.Label>Date de fin</Form.Label><br/>
                   <DatePicker selected={startDate} onChange={handleChangeDate} locale="es" />
               </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleHideForm}>Annuler</Button>
             <Button type="submit">
                 {formSubmitting ? 'Enregistrement...' : 'Enregistrer'}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>
        </>
    )
}