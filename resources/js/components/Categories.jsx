import React from "react";
import {Card, Container, Row, } from 'react-bootstrap'

function Categories(props)
{
    return (
        <>
          <Container className="mt-3">
              <div className="d-flex p-5 justify-content-center">
                  <h3>Secteurs d'investissements</h3>
              </div>

              <div className="row">
                {JSON.stringify(props.category) !== '{}' ? 
                   props.category.map((categories) => (
                    <div className="col-12 col-sm-6 col-md-4 mt-3" key={categories.id}>
                        <Card style={{ width: '16rem' }}>
                             <Card.Img variant="top" src="http://localhost:8000/images/defaults/category.png" style={{ height: '13rem' }}/>
                             <Card.Body>
                                <Card.Title className="text-center font-weight-bold">{categories.name}</Card.Title>
                                <Card.Text>{categories.description}</Card.Text>
                             </Card.Body>
                        </Card>
                    </div>
                   
                ))
                : 
                  ''}
                 
              </div>

              
          </Container>
        </>
    )
}

export default Categories