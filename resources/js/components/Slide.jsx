import React, {} from 'react'
import Slider from "react-slick";
import Button from 'react-bootstrap/Button'

function Slide(props)
{
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 10000,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    var projects = [
        {
            id: 1,
            name: 'Application de Géolocalisation',
            categorie: 'technlogie',
            img: '/images/cover/cover1.jpg',
            description: 'projet de valorisation des produits locaux et du developpment'
        },
        {
          id: 2,
          name: 'Application de Géolocalisation',
          categorie: 'technlogie',
          img: '/images/cover/cover1.jpg',
          description: 'projet de valorisation des produits locaux et du developpment'
        },
        {
          id: 3,
          name: 'usine de riz à manjo',
          categorie: 'indistrie',
          img: '/images/cover/cover1.jpg',
          description: 'projet de valorisation des produits nationaux et du developpment'
        },
    ]

    return (
        <div style={{height:'600px'}}>
        
        <Slider {...settings} >
           { projects.map(
              (project,index) => (<div key={project.id} className="card bg-dark text-white px-0">
                <img src={project.img} className="img-fluid" style={{height: '599px', width: '100%'}} alt="Responsive image"></img>
                <div className="card-img-overlay offset-1">
                  <br/><br/><br/><br/>
                   <h5 class="card-title mt-5">{project.categorie}</h5>
                   <h3 class="card-title">{project.name}</h3>
                   <p class="card-text">{project.description}</p>
                   <Button variant="primary">Je me lance</Button>
                </div>
              </div>)
           )}
        </Slider>
      </div>
      
   )
}

export default Slide