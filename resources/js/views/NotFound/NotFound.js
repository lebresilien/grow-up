import React from 'react';
import ReactDOM from 'react-dom';

function NotFound() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Erreur 404</div>

                        <div className="card-body">La page solicité n'existe pas!</div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default  NotFound;

