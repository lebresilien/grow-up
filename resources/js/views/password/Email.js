import React, {Component} from 'react';

class Email extends Component {

  constructor(props)
  {
   	 super(props);
   	 this.state = {
   	 	email: '',
   	 	formSubmitting: false
   	 };
   	 this.handleEmail = this.handleEmail.bind(this);
   	 this.handleSubmit = this.handleSubmit.bind(this);
   }

  handleEmail(e) {
  let value = e.target.value;
  this.setState({email: value});
  }

  handleSubmit(e){
  	e.preventDefault();
  	let email = this.state.email;
  	
    axios.post("/api/create", {
    	"email": email
    })
    .then(response => {
       console.log(response) ; 
    })
  }
  
   render() {

    return (
      <div className="container">
          <div className="row justify-content-center">
             <div className="col-md-8">
                <div className="card">
                    <div className="card-header">Reset Password</div>
                    <div className="card-body">
                     <form onSubmit={this.handleSubmit}>
                       <div className="form-group row">
                           <label className="col-md-4 col-form-label text-md-right">Adresse Email</label>
                           <div className="col-md-6">
                              <input  type="email" className="form-control" required autoComplete="email" onChange={this.handleEmail}/>
                           </div>
                       </div>
                       <div className="form-group row mb-0">
                          <div className="col-md-6 offset-md-4">
                              <button  className="btn btn-primary" disabled={this.state.formSubmitting ? "disabled" : ""} type="submit">
                                    Send Password Reset Link
                              </button>
                          </div>
                       </div>
                      </form> 
                    </div>
                </div>
             </div>
          </div>
      </div>
    )
  }
}

export default Email
