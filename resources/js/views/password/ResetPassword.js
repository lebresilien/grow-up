import React, {Component} from 'react';
import FlashMessage from 'react-flash-message';

class ResetPassword extends Component {

  constructor(props)
   {
   	 super(props);
   	 this.state = {
   	 	password: '',
   	 	confirm: '',
   	 	error: '',
        Message: '',
        mailSend: false,
   	 	formSubmitting: false
   	 };
   	 this.handlePassword = this.handlePassword.bind(this);
   	 this.handleConfirm = this.handleConfirm.bind(this);
   	 this.handleSubmit = this.handleSubmit.bind(this);
   }

   componentDidMount()
   {
   	console.log('bien');
   }

   handleEmail(e) {
    let value = e.target.value;
    this.setState({password: value});
   }

   handleConfirm(e) {
    let value = e.target.value;
    this.setState({confirm: value});
   }

   render() {

    let Message = this.state.Message;

    return (
      <div className="container">
          <div className="row justify-content-center">
             <div className="col-md-8">
                <div className="card">
                    <div className="card-header">New Password</div>
                    <div className="card-body">

                     {this.state.error ? <FlashMessage duration={900000} persistOnHover={true}>
                     <h5 className={"alert alert-danger"}>Error: {this.state.error}</h5>
                     <h6 style={{color: 'red'}}>{Message}</h6>
                     </FlashMessage> : ''}

                     {this.state.mailSend ? <FlashMessage duration={900000} persistOnHover={true}>
                       <h6 className={"alert alert-success"}>{Message}</h6>
                     </FlashMessage> : ''}

                     <form onSubmit={this.handleSubmit}>
                       <div className="form-group row">
                           <label className="col-md-4 col-form-label text-md-right">Password</label>
                           <div className="col-md-6">
                              <input  type="password" className="form-control" required  onChange={this.handlePassword}/>
                           </div>
                       </div>

                       <div className="form-group row">
                           <label className="col-md-4 col-form-label text-md-right">Confirm Password</label>
                           <div className="col-md-6">
                              <input  type="password" className="form-control" required onChange={this.handleConfirm}/>
                           </div>
                       </div>

                       <div className="form-group row mb-0">
                          <div className="col-md-6 offset-md-4">
                              <button  className="btn btn-primary" disabled={this.state.formSubmitting ? "disabled" : ""} type="submit">
                                    Reset Password  
                              </button>
                          </div>
                       </div>
                      </form> 
                    </div>
                </div>
             </div>
          </div>
      </div>
    )
  }
}
export default ResetPassword