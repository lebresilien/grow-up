import React, {useEffect, useState } from 'react'
import {useParams} from 'react-router-dom'
import {Container, Card, Form, Button, Col, Modal} from 'react-bootstrap'
import {FaUserEdit, FaLinkedin, FaTwitter, FaFacebook} from 'react-icons/fa'
import {MdModeEdit, MdAdd} from 'react-icons/md'
import {BsTrash} from 'react-icons/bs'


export default function CompanyDetails(props)
{
    const[showBtnTeam, setShowBtnTeam] = useState(false)

    const handleShowBtnTeam = () => setShowBtnTeam(true);
    const handleHideBtnTeam= () => setShowBtnTeam(false);

    return (
      <>
        <div className="d-flex flex-column my-3">

          <div className="py-3 px-1" style={{border: '1px solid #eee'}} onMouseEnter={handleShowBtnTeam} onMouseLeave={handleHideBtnTeam}>
              {props.data.length == 0 ? 
              <div className="d-flex justify-content-between">
                <div className="font-weight-bold">Equipe</div>
                <div className="mx-0 d-flex justify-content-center align-items-center border-fa"><MdAdd size={25} /></div>
              </div>
              :
              <div className="d-flex justify-content-between">
                  <div className="font-weight-bold">Equipe</div>
                  {showBtnTeam ? <div className="mx-0 d-flex justify-content-center align-items-center border-fa"><MdModeEdit size={25} /></div> : ''}
              </div>
            }
          </div>

        </div>
      </>
    )
}