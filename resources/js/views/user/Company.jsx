import React, {useEffect, useState, useRef} from 'react'
import {useParams} from 'react-router-dom'
import {Container, InputGroup, Card, Form, Button, Col, Modal, FormControl, Navbar, Nav, Tabs, Tab} from 'react-bootstrap'
import {MdModeEdit} from 'react-icons/md'
import Summary from '../../components/Summary'
import Presentation from '../../components/Presentation'
import Information from '../../components/Information'
import Finance from '../../components/Finance'
import Document from '../../components/Document'
import TopBar from './../../components/TopBar'
import '../../components/starter.scss'
import {AiOutlineSync} from 'react-icons/ai'
import {useDropzone} from 'react-dropzone'
import { FaFacebookSquare, FaTwitterSquare, FaLinkedin,FaCamera } from "react-icons/fa"
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import { registerLocale, setDefaultLocale } from  "react-datepicker"
import fr from 'date-fns/locale/fr'
registerLocale('fr', fr)

export default function Company() {
   const [key, setKey] = useState('home');
    const[showBackground, setShowBackground] = useState(false)
    const[showBtnProject, setShowBtnProject] = useState(false)
    const[showBtnSocial, setShowBtnSocial] = useState(false)
    const[showBtnContact, setShowBtnContact] = useState(false)
    const[showForm, setShowForm] = useState(false)
    const[showFormSocial, setShowFormSocial] = useState(false)
    const[showFormContact, setShowFormContact] = useState(false)
    const[isLoggedIn, setisLoggedIn] = useState(false);
    const[user, setUser] = useState({});
    const[concept, setConcept] = useState('');
    const[teams, setTeams] = useState([]);
    const[formSubmitting, setFormSubmitting] = useState(false);
    const[categories, setCategories] = useState([])
    const[etapes, setEtapes] = useState([])
    const[devises, setDevises] = useState([])
    const[name, setName] = useState('')
    const[spitch, setSpitch] = useState('')
    const[lieu, setLieu] = useState('')
    const[createdAt, setCreatedAt] = useState('')
    const[siteWebLink, setSiteWebLink] = useState('')
    const[facebookLink, setFacebookLink] = useState('')
    const[twitterLink, setTwitterLink] = useState('')
    const[linkedinLink, setLinkedinLink] = useState('')
    const[employees, setEmployees] = useState('')
    const[status, setStatus] = useState(false)
    const[facebookTemp, setFacebookTemp] = useState('')
    const[twitterTemp, setTwitterTemp] = useState('')
    const[linkedInTemp, setLinkedInTemp] = useState('')
    const[userEmailTemp, setUserEmailTemp] = useState('')
    const[userLocalTemp, setUserLocalTemp] = useState('') 
    const[userPhoneTemp, setUserPhoneTemp] = useState('') 
    const[step, setStep] = useState('')
    const[category_, setCategory_] = useState('')
    const[currency, setCurrency] = useState('')
    const [startDate, setStartDate] = useState(new Date());
    const[userEmail, setUserEmail] = useState('')
    const[userLieu, setUserLieu] = useState('')
    const[userID, setUserID] = useState('')
    const[userPhone, setUserPhone] = useState('')
    const[categoryId, setCategoryId] = useState('')
    const[etapeId, setEtapeId] = useState('')
    const[deviseId, setDeviseId] = useState('')
    const[company, setCompany] = useState({name: "", spitch: "", categories_id: "", lieu: "", createdAt: "", employees: "", site_web: "", logo: "", devises_id: "", etapes_id: ""})
    const[error, setError] = useState([])
    const[messageErrors, setMessageErrors] = useState('')
    const[equipeDirection, setEquipeDirection] = useState('')
    const[model, setModel] = useState('')
    const[avantage, setAvantage] = useState('')
    const[service, setService] = useState('')
    const[client, setClient] = useState('')
    const[problematique, setProblematique] = useState('')
    const[concurrent, setConcurrent] = useState('')
    const[cible, setCible] = useState('')
    const[strategy, setStrategy] = useState('')
    const[showLogo, setShowLogo] = useState(false)
    const logoRef = useRef()
    const backgroundRef = useRef()
    const formLogoRef = useRef()
    const formBackgroundRef = useRef()
    const[logo, setLogo] = useState('')
    const[cover, setCover] = useState('')
    const[video, setVideo] = useState('')

    const handleShowBackground = () => setShowBackground(true);
    const handleHideBackground = () => setShowBackground(false);

    const handleShowLogo = () => setShowLogo(true);
    const handleHideLogo = () => setShowLogo(false);

    const handleShowBtnProject = () => setShowBtnProject(true);
    const handleHideBtnProject = () => setShowBtnProject(false);

    const handleShowBtnSocial = () => setShowBtnSocial(true);
    const handleHideBtnSocial = () => setShowBtnSocial(false);

    const handleShowBtnContact = () => setShowBtnContact(true);
    const handleHideBtnContact = () => setShowBtnContact(false);


    const handleShowForm = () => setShowForm(true);
    const handleCloseForm = () => setShowForm(false);

    const handleShowFormSocial = () => {
       setFacebookTemp(facebookLink)
       setTwitterTemp(twitterLink)
       setLinkedInTemp(linkedinLink)
       setShowFormSocial(true)
    }
    const handleCloseFormSociale = () => {
       setFacebookLink(facebookTemp)
       setTwitterLink(twitterTemp)
       setLinkedinLink(linkedInTemp)
       setShowFormSocial(false)
    }
    const handleCloseFormSocial = () => setShowFormSocial(false)
   

    const handleShowFormContact = () => {
       setShowFormContact(true)
       setUserEmailTemp(userEmail)
       setUserLocalTemp(userLieu)

    }
    const handleCloseFormContact = () => {
       setUserLieu(userLocalTemp)
       setUserEmail(userEmailTemp)
       setShowFormContact(false)
    }
    
    const handleChange = (e) => {
        setCompany({...company, [e.target.name] : e.target.value });
    } 
    const handleChangeCompanyName = (e) => {
      setName(e.target.value)
    }
    const handleChangeCompanySpitch = (e) => {
      setSpitch(e.target.value)
    }
    const handleChangeCompany = (e) => {
      setName(e.target.value)
    }
    const handleChangeCompanyLieu = (e) => {
      setLieu(e.target.value)
    }
    const handleChangeCompanySiteWeb = (e) => {
      setSiteWebLink(e.target.value)
    }
    const handleChangeCompanyCreatedAt = (e) => {
      setCreatedAt(e.target.value)
    }
    const handleChangeCompanyEmployees = (e) => {
      setEmployees(e.target.value)
    }
    const handleChangeCompanyCategory = (e) => {
      setCategory_(e.target.selectedOptions[0].text)
      setCategoryId(e.target.value)
    }
    const handleChangeCompanyDevise = (e) => {
      setCurrency(e.target.selectedOptions[0].text)
      setDeviseId(e.target.value)
    }
    const handleChangeCompanyStep = (e) => {
      setStep(e.target.selectedOptions[0].text)
      setEtapeId(e.target.value)
    }

    const params  = useParams()

   const deconnexion = () => {
      let appState = {
        isLoggedIn: false,
        user: {}
      };
      localStorage["appState"] = JSON.stringify(appState);
      setisLoggedIn(JSON.parse(localStorage['appState']).isLoggedIn)
      location.href = '/'
   }
       
   useEffect(() => {
      setisLoggedIn(JSON.parse(localStorage['appState']).isLoggedIn)
      setUser(JSON.parse(localStorage['appState']).user)
      loadProject(params.id, JSON.parse(localStorage['appState']).user.access_token)
      {facebookLink || linkedinLink || twitterLink ? setStatus(true) : ''}
      /* {linkedinLink  ? setStatus(true) : ''}
      {twitterLink ? setStatus(true) : ''} */
      fecthData()
   },[userID])

   function loadProject(id, token)
   {
      const config = {
         headers: { Authorization: `Bearer ${token}` }
        };

        axios.get("/api/v1/projets/"+id, config).then((response) => {
          let res = response.data
          console.log(response.data)
          setConcept(res[0].concept)
          setTeams(res[0].equipes)
          setName(res[0].name)
          setSpitch(res[0].spitch)
          setSiteWebLink(res[0].site_web_link)
          setFacebookLink(res[0].facebook_link)
          setTwitterLink(res[0].twitter_link)
          setLinkedinLink(res[0].linkedin_link)
          setLieu(res[0].localisation)
          setEmployees(res[0].nbre_employe)
          setCategory_(res[0].category.name)
          setCategoryId(res[0].category.id)
          setDeviseId(res[0].devise.id)
          setEtapeId(res[0].etape.id)
          setCurrency(res[0].devise.name)
          setStep(res[0].etape.name)
          setUserEmail(res[0].user.email)
          setUserLieu(res[0].user.localisation)
          //setUserPhone(res[0].user.phone)
          setUserID(res[0].user.id)
          setEquipeDirection(res[0].equipe_direction)
          setProblematique(res[0].problematique)
          setService(res[0].product_service)
          setCible(res[0].market_target)
          setModel(res[0].business_model)
          setClient(res[0].client)
          setStrategy(res[0].sale_marketing_strategy)
          setConcurrent(res[0].concurrent)
          setAvantage(res[0].avantage_concurrentiel)
          setLogo(res[0].logo)
          setCover(res[0].cover)
          setVideo(res[0].video)
         
        })
       .catch(errors => {
        //alert(errors)
        }) 
   }

   function fecthData(){
      axios.get("/api/v1/categories").then((response) => {
          setCategories(response.data)
      })
      axios.get("/api/v1/devises").then((response) => {
         setDevises(response.data)
     })
     axios.get("/api/v1/etapes").then((response) => {
         setEtapes(response.data)
     })
   }

   const handleChangeFacebook = (e) => {
      setFacebookLink(e.target.value)
   }
   const handleChangeTwitter = (e) => {
      setTwitterLink(e.target.value)
   }
   const handleChangeLinkedIn = (e) => {
      setLinkedinLink(e.target.value)
   }

   const submitFormSocial = (e) => {
      e.preventDefault() 
      setFormSubmitting(true)
      const config = {
         headers: { Authorization: `Bearer ${user.access_token}` }
      };
      let data = {"facebook_link": facebookLink, "twitter_link": twitterLink, "linkedin_link": linkedinLink}

      axios.put("/api/v1/projets/"+params.id, data, config).then((response) => {
         setFormSubmitting(false)
         {facebookLink || twitterLink || linkedinLink ? setStatus(true) : setStatus(false)}
         setShowFormSocial(false)
       })
      .catch(errors => {
         setFormSubmitting(true)
       alert(errors)
       }) 
      
   }

   const handleSubmitCompany = (e) => {
      setError([])
      setMessageErrors('')
      e.preventDefault() 
      setFormSubmitting(true)
      const config = {
         headers: { Authorization: `Bearer ${user.access_token}` }
      };
     let data = {"name": name, "spitch": spitch, "lieu": lieu, "site_web_link": siteWebLink, "created_at": createdAt, "category_id": categoryId, "etape_id": etapeId, "devise_id": deviseId, "employees": employees } 
     axios.put("/api/v1/projets/"+params.id, data, config).then((response) => {
         setFormSubmitting(false)
         showForm(false)
       })
      .catch(errors => {
         setFormSubmitting(false)
         if(errors.response) {
            let err = errors.response.data;
            setError(err.errors)
            setMessageErrors(err.message)
         }
         else if (error.request) {
            let err = errors.request;
            setError(err)
         }
         else{
            let err = errors.message;
            setError(err)
         }
       })  
   }

   const handleChangeUserEmail = (e) => {
      setUserEmail(e.target.value)
   }
   const handleChangeUserPhone = (e) => {
      setUserEmail(e.target.value)
   }
   const handleChangeUserLocal = (e) => {
      setUserLieu(e.target.value)
   }

   const submitFormContact = (e) => {
      e.preventDefault() 
      setFormSubmitting(true)
      setError([])
      setMessageErrors('') 
      
      const config = {
         headers: { Authorization: `Bearer ${user.access_token}` }
      };
     let data = {"email": userEmail, "localisation": userLieu } 
     
      axios.put("/api/v1/users/"+params.id, data, config).then((response) => {
         setFormSubmitting(false)
         showFormContact(false)
       })
      .catch(errors => {
         setFormSubmitting(false)
         if(errors.response) {
            let err = errors.response.data;
            setError(err.errors)
            setMessageErrors(err.message)
         }
         else if (error.request) {
            let err = errors.request;
            setError(err)
         }
         else{
            let err = errors.message;
            setError(err)
         }
       })   
   }

   const uploadLogo = () => {
      logoRef.current.click()
   }
   const uploadBackground = () => {
      backgroundRef.current.click()
   }

   function handleUploadLogo(e){    
      
      let files = e.target.files
      if (!files.length) return
      let file = files[0]
      let formData = new FormData()
      formData.append('file', file)
      formData.append('id', params.id)
      const config = {
         headers: { "Content-type": "multipart/form-data", Authorization: `Bearer ${user.access_token}`} 
      };
      axios.post("/api/v1/projets.logo", formData, config).then((response) => {
         setLogo(response.data.logo)
      })
          
   }

   function handleUploadBackground(e){    
      
      let files = e.target.files
      if (!files.length) return
      let file = files[0]
      let formData = new FormData()
      formData.append('file', file)
      formData.append('id', params.id)
      const config = {
         headers: { "Content-type": "multipart/form-data", Authorization: `Bearer ${user.access_token}`} 
      };
      axios.post("/api/v1/projets.cover", formData, config).then((response) => {
         setCover(response.data.cover)
      })
          
   }


    return (
        <>
          <TopBar isLoggedIn={isLoggedIn} user={user} deconnexion={deconnexion}/>
          <div className="row justify-content-center py-5 bg-white">
              <div className="col-sm-9">
                 <div className="h-100">
                    <div className="row" style={{border:'1px solid black'}} onMouseEnter={handleShowBackground} onMouseLeave={handleHideBackground}>
                        <Card className="text-white" style={{width:'100%'}} >
                             <Card.Img src={cover} style={{height:'315px'}} width="100%" />
                             {showBackground ? 
                               <Card.ImgOverlay>
                                  <div className="d-flex justify-content-center align-items-center bg-dark py-2 pointer" onClick={uploadBackground}> <FaCamera /></div>
                               </Card.ImgOverlay>
                              :
                              ''
                             }
                        </Card>
                         {/* {showBackground ? <div className="d-flex image-background justify-content-center align-items-center"> <FaCamera /></div> : ''}
                         <img src="/images/defaults/background.jpg" style={{height:'315px'}} width="100%"/> */}
                    </div>

                    <div className="row my-1">
                       <div className="col-md-4 col-12"> 
                       <div className="content-company bg-white p-2" onMouseEnter={handleShowBtnProject} onMouseLeave={handleHideBtnProject}>
                          <div className="row py-2">
                             <div className="offset-4 col-sm-6 col-6" onMouseEnter={handleShowLogo} onMouseLeave={handleHideLogo}>
                              <Card className="text-white" style={{width:'100px'}}>
                               <Card.Img src={logo} />
                                {showLogo ? 
                                  <Card.ImgOverlay >
                                   <div className="d-flex justify-content-center align-items-center bg-dark pointer" onClick={uploadLogo}> <FaCamera /> Upload</div>
                                  </Card.ImgOverlay> :
                                ''
                                }
                               </Card>
                             </div>
                             {showBtnProject ? <div className="col-sm-2 col-2"><MdModeEdit className="pointer" onClick={handleShowForm}/></div> : '' }
                          </div>
                          <div className="d-flex justify-content-center font-weight-bold p-1 text-uppercase">
                                 {name}
                          </div>
                          <div className="d-flex text-center py-1">
                             <blockquote><q>{spitch}</q></blockquote>
                          </div>
                          <div className="d-flex flex-column pb-2">

                            <div className="d-flex justify-content-between separation">
                               <div className="">Etape</div><div className="">{step}</div>
                             </div>

                             <div className="d-flex justify-content-between separation">
                               <div className="">Secteur d'activité</div><div>{category_}</div>
                             </div>

                             <div className="d-flex justify-content-between separation">
                               <div className="">Lieu</div><div>{lieu}</div>
                             </div>

                             <div className="d-flex justify-content-between separation">
                               <div className="">Devise</div><div className="">{currency}</div>
                             </div>

                             <div className="d-flex justify-content-between separation">
                               <div className="">Fondée</div><div className="">{createdAt ? createdAt : '---'}</div>
                             </div>

                             <div className="d-flex justify-content-between separation">
                               <div className="">Employé</div><div className="">{employees ? employees : '---' }</div>
                             </div>

                             <div className="d-flex justify-content-between py-2">
                               <div className="">Site Web</div><div className="">{siteWebLink ? siteWebLink :  '---'}</div>
                             </div>
                          </div>
                       </div>
                       <div className="d-flex flex-column content-social p-2" onMouseEnter={handleShowBtnSocial} onMouseLeave={handleHideBtnSocial}>
                            <div className="d-flex justify-content-between">
                               <div className="font-weight-bold">Presence sur les reseaux sociaux</div>
                               {showBtnSocial ? <div className=""><MdModeEdit className="pointer" onClick={handleShowFormSocial} /></div> : ''}
                            </div>
                            {!status ?
                            <div className="d-flex justify-content-center">
                                Ajoutez les liens vers les profils sociaux de votre entreprise.
                            </div>
                            :
                            <div className="d-flex flex-row justify-content-center">
                                {facebookLink ? <div className="p-1"><a href={"https://facebook.com/"+ facebookLink }><FaFacebookSquare size={25} /></a></div> : ''}
                                {linkedinLink ? <div className="p-1"><a href={"https://linkedin.com/"+ linkedinLink}><FaLinkedin size={25} /></a></div> : ''}
                                {twitterLink ? <div className="p-1"><a href={"https://twitter.com/"+ twitterLink}><FaTwitterSquare size={25} /></a></div> : ''}
                            </div>
                           }
                             
                        </div>

                        <div className="d-flex flex-column content-contact p-2" onMouseEnter={handleShowBtnContact} onMouseLeave={handleHideBtnContact}>
                            <div className="d-flex justify-content-between p-1">
                               <div className="font-weight-bold">Contact</div>
                               {showBtnContact ? <div className=""><MdModeEdit className="pointer" onClick={handleShowFormContact} /></div> : ''}
                            </div>
                            <div className="p-1">
                                678660800
                            </div>
                            <div className="p-1">
                                {userEmail}
                            </div>
                            <div className="p-1">
                                {userLieu}
                            </div>    
                        </div>

                       </div>
                       <div className="py-2 col-md-8 col-12">
                           
                            {/*  <Navbar collapseOnSelect expand="lg">
                                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                                <Navbar.Collapse id="responsive-navbar-nav">
                                   <Nav className="">
                                      <Nav.Link href="#overview">Vue d'ensemble</Nav.Link>
                                      <Nav.Link href="#pricing">Présentation</Nav.Link>
                                      <Nav.Link href="#supplement">Informations sue l'entreprise</Nav.Link>
                                      <Nav.Link href="#pricing">Finances</Nav.Link>
                                      <Nav.Link href="#pricing">Documents</Nav.Link>
                                   </Nav>
                                </Navbar.Collapse>
                             </Navbar> */}
                             <Tabs activeKey={key} onSelect={(el) => setKey(el)}> 

                                 <Tab eventKey="home" title="Vue d'ensemble">
                                    <Summary  user={user} project={params.id} concept={concept} 
                                              teams={teams} video={video} 
                                    />
                                 </Tab>

                                 <Tab eventKey="Présentation" title="Présentation">
                                    <Presentation />
                                 </Tab>

                                  <Tab eventKey="Informations" title="Informations sur l'entreprise">
                                    <Information  model={model} service={service} strategy={strategy} cible={cible}
                                                  concurrent={concurrent} equipe={equipeDirection} 
                                                  client={client} avantage={avantage} problematique={problematique}
                                                  projectID={params.id}  user={user} 
                                    />
                                 </Tab>

                                  <Tab eventKey="Finances" title="Finances">
                                    <Finance />
                                 </Tab>
                                  <Tab eventKey="Documents" title="Documents">
                                    <Document/>
                                 </Tab>
                             </Tabs>
                           
                           
                           
                       </div>
                    </div>
                 </div>
              </div>
               
          </div>

          <Modal show={showForm} onHide={handleCloseForm}>
            <Modal.Header closeButton>
              <Modal.Title>Informations de base de votre entreprise</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitCompany}>
            <Modal.Body>
                 <div className="d-flex flex-column">
                     {messageErrors ? <div className="p-2 justify-content-center"><h5 className="alert alert-danger">{messageErrors}</h5></div> : ''}
      
                     {error ?
                     <div className="p-2 justify-content-center">
                        <ul className="list-group">
                           {error.name ? <li className="list-group-item alert alert-danger">{error.name}</li> : '' }
                           {error.spitch ? <li className="list-group-item alert alert-danger">{error.spitch}</li> : '' }
                           {error.lieu ? <li className="list-group-item alert alert-danger">{error.lieu}</li> : '' }
                           
                        </ul>
                     </div>
                     :
                     ''
                     }

                     
                  </div>
                  <Form.Group>
                     <Form.Label>Nom Entreprise <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="text"  name="name" value={name} onChange={handleChangeCompanyName}/>
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Spitch <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  as="textarea"  name="spitch" value={spitch} onChange={handleChangeCompanySpitch} rows={3}/>
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Localisation <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="text"  name="lieu" value={lieu} onChange={handleChangeCompanyLieu}/>
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Etape <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  as="select"  name="etapes_id"  onChange={handleChangeCompanyStep}>
                        {etapes.length !== 0 ?
                         etapes.map((etape, index) => (
                           <option value={etape.id} selected={etape.name == step ? 'selected':''}>{etape.name}</option>
                         ))
                         :
                         ''
                        }
                     </Form.Control>
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Categorie <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  as="select"  onChange={handleChangeCompanyCategory}>
                        {categories.length !== 0 ?
                         categories.map((category, index) => (
                           <option value={category.id}  selected={category.name == category_ ? 'selected':''}>{category.name}</option>
                         ))
                         :
                         ''
                        }
                     </Form.Control>
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Devise</Form.Label>
                     <Form.Control  as="select"  name="devise_id" onChange={handleChangeCompanyDevise}>
                        {devises.length !== 0 ?
                         devises.map((devise, index) => (
                           <option value={devise.id} selected={devise.name == currency ? 'selected':''}>{devise.name}</option>
                         ))
                         :
                         ''
                        }
                     </Form.Control>
                  </Form.Group>
                   <Form.Group>
                     <Form.Label>Date Création</Form.Label> <br />
                     <DatePicker selected={createdAt ? createdAt : startDate} name="createdAt" onChange={handleChangeCompanyCreatedAt} locale="fr" />
                   </Form.Group>
                   <Form.Group>
                     <Form.Label>Nombre d'employés</Form.Label>
                     <Form.Control  type="number" min={0}  name="employees" value={employees} onChange={handleChangeCompanyEmployees} />
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Site Web</Form.Label>
                     <Form.Control  type="text"  name="site_web" value={siteWebLink} onChange={handleChangeCompanySiteWeb} />
                  </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleCloseForm}>Annuler</Button>
             <Button type="submit" disabled={formSubmitting ? 'disabled': ''}>
                {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormSocial} onHide={handleCloseFormSociale}>
            <Modal.Header closeButton>
              <Modal.Title>Presence sur les reseaux sociaux</Modal.Title>
            </Modal.Header>
            <Form onSubmit={submitFormSocial}>
            <Modal.Body>
                  <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                     <InputGroup.Text> https://linkedin.com/</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl  value={linkedinLink} onChange={handleChangeLinkedIn} />
                  </InputGroup>

                  <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                     <InputGroup.Text> https://twitter.com/</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl  value={twitterLink} onChange={handleChangeTwitter} />
                  </InputGroup>

                  <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                     <InputGroup.Text> https://facebook.com/</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl  value={facebookLink} onChange={handleChangeFacebook} />
                  </InputGroup>
               
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleCloseFormSociale}>Annuler</Button>
             <Button type="submit">
               {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Modal show={showFormContact} onHide={handleCloseFormContact}>
            <Modal.Header closeButton>
              <Modal.Title>Informations Personnells</Modal.Title>
            </Modal.Header>
            <Form onSubmit={submitFormContact}>
            <Modal.Body>

                  <div className="d-flex flex-column">
                     {messageErrors ? <div className="p-2 justify-content-center"><h5 className="alert alert-danger">{messageErrors}</h5></div> : ''}
      
                     {error ?
                     <div className="p-2 justify-content-center">
                        <ul className="list-group">
                           {error.localisation ? <li className="list-group-item alert alert-danger">{error.localisation}</li> : '' }
                           {error.phone ? <li className="list-group-item alert alert-danger">{error.phone}</li> : '' }
                           {error.email ? <li className="list-group-item alert alert-danger">{error.email}</li> : '' }
                           
                        </ul>
                     </div>
                     :
                     ''
                     }

                     
                  </div>
                  <Form.Group>
                    <Form.Label>Telephone</Form.Label>
                     <Form.Control  type="text" value="678660800" onChange={handleChangeUserPhone}/>
                    </Form.Group>

                    <Form.Group>
                     <Form.Label>Email <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="email" value={userEmail} onChange={handleChangeUserEmail}/>
                    </Form.Group>

                    <Form.Group>   
                     <Form.Label>Boite Postal <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="text" value={userLieu} onChange={handleChangeUserLocal}/>
                    </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleCloseFormContact}>Annuler</Button>
             <Button type="submit">
                {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>

          <Form style={{visibility: 'hidden'}}>
             <Form.Group>
               <input type="file" ref={logoRef}  onChange={() => handleUploadLogo(event)} />
             </Form.Group>
          </Form>

          <Form style={{visibility: 'hidden'}}>
             <Form.Group>
               <input type="file" ref={backgroundRef} name="file" onChange={() => handleUploadBackground(event)} />
             </Form.Group>
          </Form>


        </>
    )
}