import React, {useEffect, useState, useRef} from 'react'
import {useParams} from 'react-router-dom'
import {Container, Card, Form, Button, Col, Modal} from 'react-bootstrap'
import {FaUserEdit, FaLinkedin, FaTwitter, FaFacebook} from 'react-icons/fa'
import {MdModeEdit, MdAdd} from 'react-icons/md'
import {BsTrash} from 'react-icons/bs'
import {AiOutlineSync} from 'react-icons/ai'
import MouseTooltip from 'react-sticky-mouse-tooltip'
import TopBar from './../../components/TopBar'
import '../../components/starter.scss'

export default function Profile() {
    const[show, setShow] = useState(false)
    const[showContactInfo, setShowContactInfo] = useState(false)
    const[ShowButtonInfo, setShowButtonInfo] = useState(false)
    const[showFormUser, setShowFormUser] = useState(false)
    const[isMouseTooltipVisible, setIsMouseTooltipVisible] = useState(false)
    const[showButtonAdd, setShowButtonAdd] = useState(false)
    const[showForm, setShowForm] = useState(false)
    const[showBtnBio, setShowBtnBio] = useState(false)
    const[showBio, setShowBio] = useState(false)
    const[showExp, setShowExp] = useState(false)
    const[isLoggedIn, setisLoggedIn] = useState(false);
    const[user, setUser] = useState({});
    const[Bio, setBio] = useState("")
    const[showBtnExp, setShowBtnExp] = useState(false)
    const[experiences, setExperiences] = useState([])
   const[loading, setLoading] = useState(false);
   const[siteWeb, setSiteWeb] = useState('');
   const[facebookLink, setFacebookLink] = useState('');
   const[twitterLink, setTwitterLink] = useState('');
   const[linkedLink, setLinkedLink] = useState('');
   const[name, setName] = useState('');
   const[surname, setSurname] = useState('');
   const[localisation, setLocalisation] = useState('');
   const[formSubmitting, setFormSubmitting] = useState(false);
   const[selectedFile, setSelectedFile] = useState('');
   const[logo, setLogo] = useState('')
   const[categories, setCategories] = useState([])
   const[etapes, setEtapes] = useState([])
   const[devises, setDevises] = useState([])
   const[companyName, setCompanyName] = useState('')
   const[companyConcept, setCompanyConcept] = useState('')
   const[companyLocal, setCompanyLocal] = useState('')
   const[companyDevise, setCompanyDevise] = useState('')
   const[companyEtape, setCompanyEtape] = useState('')
   const[companyCategory, setCompanyCategory] = useState('')
   const[errors, setErrors] = useState([])
   const[messageError, setMessageError] = useState('')
   const[compteur, setCompteur] = useState(0)

   const deconnexion = () => {
      let appState = {
        isLoggedIn: false,
        user: {}
      };
      localStorage["appState"] = JSON.stringify(appState);
      setisLoggedIn(JSON.parse(localStorage['appState']).isLoggedIn)
      location.href = '/'
    }
       
    useEffect(() => {
      setisLoggedIn(JSON.parse(localStorage['appState']).isLoggedIn)
      setUser(JSON.parse(localStorage['appState']).user)
      fetchUserData(JSON.parse(localStorage['appState']).user.id, JSON.parse(localStorage['appState']).user.access_token)
      fecthData()
    },[loading])
    
  

    async function fetchUserData(id, token){

        const config = {
         headers: { Authorization: `Bearer ${token}` }
        };

        await axios.get("/api/v1/users/"+id, config).then((response) => {
          
          setBio(response.data[0].bio)
          setExperiences(response.data[0].experiences)
          setName(response.data[0].name)
          setSurname(response.data[0].last_name)
          setLocalisation(response.data[0].localisation)
          setSiteWeb(response.data[0].site_web_link)
          setLinkedLink(response.data[0].linkedin_link)
          setFacebookLink(response.data[0].facebook_link)
          setTwitterLink(response.data[0].twitter_link)
          setLogo(response.data[0].logo)
        })
       .catch(errors => {
        alert(errors)
        }) 

    }

    function fecthData(){
       axios.get("/api/v1/categories").then((response) => {
          setCategories(response.data)
       })
       axios.get("/api/v1/devises").then((response) => {
         setDevises(response.data)
      })
      axios.get("/api/v1/etapes").then((response) => {
         setEtapes(response.data)
      })
    }


   const fileRef = useRef()
   const formRef = useRef()

    let param  = useParams()

    const mouseEnter = () => {
        setShow(true)
    }
    const mouseLeave = () => {
        setShow(false)
    }

    const handleClikUserEdit = () => {
        setIsMouseTooltipVisible(true)
        setShowFormUser(true)
        setIsMouseTooltipVisible(false)
    }

    const handleClikButtonInfo = () => {
        setShowContactInfo(true)
    }

    const handleHideInfo = () => {
        setShowContactInfo(false)
    }

    const handleRemoveUserEdit = () => {
        setIsMouseTooltipVisible(true)
        setShowFormUser(false)
        setIsMouseTooltipVisible(false)
    }

    const mouseEnterContact = () => {
        setShowButtonInfo(true)
    }
    const mouseLeaveContact = () => {
        setShowButtonInfo(false)
    }

    const mouseEnterCompany = () => {
        setShowButtonAdd(true)
    }
    const mouseLeaveCompany = () => {
        setShowButtonAdd(false)
    }

    const handleShowForm = () => setShowForm(true);
    const handleCloseForm = () => setShowForm(false);

    const handleShowBtnBio = () => setShowBtnBio(true);
    const handleHideBtnBio = () => setShowBtnBio(false);

    const handleClickBtnBio = () => setShowBio(true);
    const handleClickHideBio = () => setShowBio(false);

    const handleChange = (e)  => {
        setBio(e.target.value)
    }

    const handleShowBtnExp = () => setShowBtnExp(true);
    const handleHideBtnExp = () => setShowBtnExp(false);

    const handleClickHideExp = () => setShowExp(true);
    const handleCloseExp = () => setShowExp(false);

    function removeExperience(e,index) {
        
      let filteredExperience = experiences.filter((item,itemIndex) => index !== itemIndex)
      let data = {"experience": filteredExperience}
      const config = {
         headers: { Authorization: `Bearer ${user.access_token}` }
      };
      axios.put("/api/v1/users/"+user.id, data, config).then((response) => {
         setExperiences(filteredExperience)
         setShowExp(false)
      })
      .catch(errors => {
        alert(errors)
      })
        
    }

    const addFormExp = () => {
        let experience = {"name": "", "role": "", "started_year": "", "ended_year": ""}
        setExperiences(experiences => [...experiences, experience])
    }

    function handleChangeName(index, e) {
      const updatedExperience = [...experiences]
      updatedExperience[index] = {...updatedExperience[index], name: e.target.value}
      setExperiences(updatedExperience) 
    }

    function handleChangeRole(index, e) {
        const updatedExperience = [...experiences]
        updatedExperience[index] = {...updatedExperience[index], role: e.target.value}
        setExperiences(updatedExperience) 
    }

    function handleChangeDebut(index, e) {
        const updatedExperience = [...experiences]
        updatedExperience[index] = {...updatedExperience[index], started_year: e.target.value}
        setExperiences(updatedExperience) 
    }

    function handleChangeFin(index, e) {
        const updatedExperience = [...experiences]
        updatedExperience[index] = {...updatedExperience[index], ended_year: e.target.value}
        setExperiences(updatedExperience) 
     }

     const uploadPhoto = () => {
        fileRef.current.click()
     }

     function handleUploadLogo(e){    
      
      let files = e.target.files
      if (!files.length) return
      let file = files[0]

     /*  let fileReader = new FileReader();
      fileReader.onload = function(progressEvent) {
        let url = fileReader.result;
      } */

      let formData = new FormData()
      formData.append('file', file)
      const config = {
         headers: { "Content-type": "multipart/form-data", Authorization: `Bearer ${user.access_token}`} 
      };
      axios.post("/api/v1/users.logo", formData, config).then((response) => {
         console.log(response)
         setLogo(response.data[0])
      }) 
          
      }

      function createImage(file) {
         let reader = new FileReader();
         reader.onload = (e) => { setSelectedFile(e.target.result)}
         reader.readAsDataURL(file);
      }

       const handleSubmitFile = (e) => {
         e.preventDefault()
         let data = {file: selectedFile}
         axios.post("/api/v1/users.logo", data, config).then((response) => {
          console.log(response)
         })
         .catch(errors => {
           console.log(errors)
         }) 
      
      }  
     

     const saveExperience = () => {
      let data = {"experience": experiences}
      const config = {
         headers: { Authorization: `Bearer ${user.access_token}` }
        };
      axios.put("/api/v1/users/"+user.id, data, config).then((response) => {
         setShowExp(false)
      })
      .catch(errors => {
        alert(errors)
      })
     } 

     const handleSiteWeb = (e) => {
        setSiteWeb(e.target.value)
     }
     const handleFacebookLink = (e) => {
      setFacebookLink(e.target.value)
     }
     const handleTwitterLink = (e) => {
      setTwitterLink(e.target.value)
     }
     const handleLinkedLink = (e) => {
      setLinkedLink(e.target.value)
     }

     const handleChangeNom = (e) => {
      setName(e.target.value)
     }
     const handleChangeSurname = (e) => {
      setSurname(e.target.value)
     }
     const handleChangeLocalisation = (e) => {
      setLocalisation(e.target.value)
     }

     const handleSubmitContact = (e) => {

        setFormSubmitting(true)
        e.preventDefault()

        let data = {'site_url': siteWeb, 'facebook_link': facebookLink,'twitter_link': twitterLink,'linked_link': linkedLink}
        const config = {
         headers: { Authorization: `Bearer ${user.access_token}` }
        };

        axios.put("/api/v1/users/"+user.id, data, config).then((response) => {
         setFormSubmitting(false)
        })
       .catch(errors => {
        alert(errors)
        })
     } 

     const handleSubmitProfil = (e) => {

      setFormSubmitting(true)
      e.preventDefault()

      let data = {'name': name, 'last_name': surname,'localisation': localisation}
      const config = {
       headers: { Authorization: `Bearer ${user.access_token}` }
      };

      axios.put("/api/v1/users/"+user.id, data, config).then((response) => {
       setFormSubmitting(false)
       setShowFormUser(false)
      })
     .catch(errors => {
      alert(errors)
      })
   } 

   const handleSubmitBio = (e) => {

      setFormSubmitting(true)
      e.preventDefault()

      let data = {'bio': Bio}
      const config = {
       headers: { Authorization: `Bearer ${user.access_token}` }
      };

      axios.put("/api/v1/users/"+user.id, data, config).then((response) => {
       setFormSubmitting(false)
       setShowBio(false)
      })
     .catch(errors => {
      alert(errors)
      })
   } 

   const handleCompanyName = (e)  => {
      setCompanyName(e.target.value)
   }
   const handleCompanyConcept = (e)  => {
      setCompanyConcept(e.target.value)
   }
   const handleCompanyLocal = (e)  => {
      setCompanyLocal(e.target.value)
   }
   const handleCompanyDevise = (e)  => {
      setCompanyDevise(e.target.value)
   }
   const handleCompanyEtape = (e)  => {
      setCompanyEtape(e.target.value)
   }
   const handleCompanyCategory = (e)  => {
      setCompanyCategory(e.target.value)
   }

   const handleSubmitCompany = (e) => {

      e.preventDefault()
      setFormSubmitting(true)
   
      let data = {
         'name': companyName, 'concept': companyConcept,'local': companyLocal,
         'devises_id': companyDevise, 'categories_id': companyCategory,'etapes_id': companyEtape,
      }

      const config = {
         headers: { Authorization: `Bearer ${user.access_token}` }
        };

      axios.post("/api/v1/projets", data, config).then((response) => {
         setFormSubmitting(false)
         setShowForm(false)
      })
      .catch(errors => {
         setFormSubmitting(false)
         if (errors.response) {
            let err = errors.response.data;
            setErrors(err.errors)
            setMessageError(err.message)
         }
         else if (error.request) {
            let err = errors.request;
            setErrors(err)
         }
         else {
            let err = errors.message;
            setErrors(err)
         }
      }) 

    }
        
     

    return (
        <>
         <TopBar isLoggedIn={isLoggedIn} user={user} deconnexion={deconnexion}/>
          <Container>
            {!showFormUser ? 
             <div className="d-flex flex-row justify-content-between py-3" onMouseEnter={mouseEnter} onMouseLeave={mouseLeave}>
                <div className="d-flex py-2 user-info">
                    <div className="">
                       <Card.Img id="user-image"  src={`/../${logo}`} className="rounded-circle user-image" />
                    </div>
                    <div className="mx-2">
                       <span><b>{name}{''}{surname}</b></span><br/>
                       <span>{localisation}</span>
                    </div> 
                </div>
                <div className="p-2">{show ? <FaUserEdit className="fa-edit-user" onClick={handleClikUserEdit}/> : ''}</div>
             </div>
             : 
             <Card className="p-3">
                 <div className="row no-gutters">
                    <div className="col-sm-12 col-12 col-md-2">
                       <img src={`/../${logo}`} className="card-img user-update-img" />
                       <div className="d-flex div_delete justify-content-center align-items-center">
                          <BsTrash />
                       </div>
                       <div className="d-flex div_update justify-content-center" onClick={uploadPhoto}>
                           <AiOutlineSync /><span className="text-image">Charger L'image</span>
                       </div>
                    </div>
                    <div className="col-sm-12 col-12 col-md-10">
                        <Card.Body>
                           <Form onSubmit={handleSubmitProfil}>
                              <Form.Group>
                                 <Form.Label>Nom</Form.Label>
                                 <Form.Control type="text" value={name}  onChange={handleChangeNom} style={{textTransform: 'capitalize'}}/>
                              </Form.Group>
                              <Form.Group>
                                 <Form.Label>Prenom</Form.Label>
                                 <Form.Control type="text" value={surname}  onChange={handleChangeSurname} style={{textTransform: 'capitalize'}}/>
                              </Form.Group>
                              <Form.Group>
                                 <Form.Label>Lieu</Form.Label>
                                 <Form.Control type="text" value={localisation}  onChange={handleChangeLocalisation} style={{textTransform: 'capitalize'}}/>
                              </Form.Group>
                              <Form.Row>
                                 <Form.Group as={Col}></Form.Group>
                                 <Form.Group as={Col}>
                                   <div className="d-flex flex-row justify-content-end">
                                      <div className="p-1">
                                          <Button variant="outline-primary" onClick={handleRemoveUserEdit} >ANNULER</Button>
                                      </div>
                                      <div className="p-1">
                                          <Button type="submit" disable={formSubmitting ? 'disables': ''}>
                                            {formSubmitting ? <span className="font-weight-bold ">ENREGISTREMENT...</span> : <span className="font-weight-bold ">ENREGISTRER</span>}
                                          </Button>
                                      </div>
                                    </div> 
                                 </Form.Group>
                              </Form.Row>
                           </Form>
                           <MouseTooltip visible={isMouseTooltipVisible} offsetX={15} offsetY={10}>
                               <span>Chargement...</span>
                           </MouseTooltip>
                        </Card.Body>
                    </div>
                 </div>
             </Card>
             
            }

            <div className="row">
                <div className="col-sm-8">
                 <div className="d-flex flex-column">
                  {!showBio ? 
                    <div className="">
                    <div className="d-flex justify-content-between" onMouseEnter={handleShowBtnBio} onMouseLeave={handleHideBtnBio}>
                       <div className="my-2"><b>Biobiographie </b></div>
                       {showBtnBio ? <div><MdModeEdit className="edit-contact-info" onClick={handleClickBtnBio} /></div> : ''}
                    </div>
                    <div className=" p-1">{Bio}</div>
                    </div>
                    
                  :
                    <div className="follow">
                       <div className="justify-content-center mx-2 p-3"> 
                        <Form onSubmit={handleSubmitBio}>
                           <Form.Group>
                               <Form.Label>Biobiographie</Form.Label>
                               <Form.Control as="textarea" rows="7" name="bibiographie" value={Bio} onChange={handleChange} />
                           </Form.Group>
                           <div className="d-flex flex-row justify-content-end p-3">
                              <Button variant="btn btn-outline-primary" className="mx-1" onClick={handleClickHideBio}>Annuler</Button>
                              <Button type="submit" disabled={formSubmitting ? 'disables':''}>
                                 {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
                                 </Button>
                           </div>
                        </Form>
                       </div>
                    </div>
                  }

                  {!showExp ? 
                   <div className="">
                    <div className="d-flex justify-content-between" onMouseEnter={handleShowBtnExp} onMouseLeave={handleHideBtnExp}>
                      <div className="my-2"><b>Experience</b></div>
                      {showBtnExp ? <div><MdModeEdit className="edit-contact-info"  onClick={handleClickHideExp}/></div> : ''}
                    </div>
                    {experiences.length !== 0 ? 
                         experiences.map((exp, index) => (
                            <div className="d-flex flex-column py-1" key={index}>
                              <span className="">{exp.name}</span>
                              <span className="">{exp.role}</span>
                              <span className="">{exp.started_year} - {exp.ended_year}</span>
                            </div>  
                        ))
                     : 
                     ''}
                   </div> 
                   :
                   <div className="follow container-fluid mb-5">
                       <div className="py-2"><b>Experience</b></div>
                       <Form>
                        {experiences.map((exp, index) => (
                         <div className="d-flex flex-column py-1 follow px-5 my-2" key={exp.id} data-key={exp.id}> 
                            <Form.Group>
                               <Form.Label>Nom de l'Entreprise</Form.Label>
                               <Form.Control type="text" name="name" value={exp.name} onChange={() => handleChangeName(index,event)} />
                            </Form.Group>
                            <Form.Group>
                               <Form.Label>role</Form.Label>
                               <Form.Control type="text" name="role" value={exp.role} onChange={() => handleChangeRole(index,event)}/>
                            </Form.Group>
                            <Form.Row>
                             <Form.Group as={Col}>
                               <Form.Label>Année debut</Form.Label>
                               <Form.Control type="text" value={exp.started_year} name="started_year"  onChange={() => handleChangeDebut(index,event)} />
                             </Form.Group>
                             <Form.Group as={Col} >
                                <div className="offset-5">
                                  <Form.Control style={{visibility:"hidden"}} type="text" />
                                  <Form.Label >à</Form.Label>
                               </div>
                             </Form.Group>
                             <Form.Group as={Col}>
                               <Form.Label>Année de fin</Form.Label>
                               <Form.Control type="text" value={exp.ended_year} name="ended_year" onChange={() => handleChangeFin(index,event)}/>
                             </Form.Group>
                            </Form.Row>
                            <div className="d-flex py-1">
                              <Button variant="outline-danger" onClick={() => removeExperience(event, index)}>Supprimer</Button>
                            </div>
                         </div> 
                        ))}
                        </Form>
                        <div className="">
                           <br/>
                           <p className="font-weight-normal pointer" onClick={addFormExp}>Ajouter une experience</p>
                        </div>
                        <div className="d-flex justify-content-end mb-3">
                           <Button variant="btn btn-outline-primary mx-1" onClick={handleCloseExp}>Annuler</Button>
                           <Button variant="primary" onClick={saveExperience} >Enregistrer</Button>
                        </div>
                   </div>
                  }
                 </div>
                </div>
                <div className="col-sm-4">
                 <div className="d-flex flex-column">
                  {!showContactInfo ? 
                   <div className="p-2" onMouseEnter={mouseEnterContact} onMouseLeave={mouseLeaveContact}> 
                      <div className="d-flex flex-row justify-content-between">
                         
                        <div className="my-2"><b>Contact </b></div>
                        {ShowButtonInfo ? <div><MdModeEdit className="edit-contact-info" onClick={handleClikButtonInfo} /></div> : ''} 
                      </div>
                      <div className="d-flex flex-row justify-content-between follow">
                        <div className="p-2"><b>Follow</b></div>
                        <div className="p-2"><FaLinkedin /></div>
                      </div>
                   </div> 
                   :
                   <div className="follow p-2">
                       <p><b>Contact </b></p>
                        <form onSubmit={handleSubmitContact}>
                          <div className="form-group">
                             <label>Site Web</label>
                             <input type="text" className="form-control" onChange={handleSiteWeb} value={siteWeb}></input>
                          </div>
                          <div className="form-group">
                             <div className="">
                                <label className="sr-only">Site</label>
                                <div className="input-group mb-2">
                                   <div className="input-group-prepend">
                                       <div className="input-group-text"><FaLinkedin /></div>
                                   </div>
                                   <input type="text" className="form-control" onChange={handleLinkedLink} value={linkedLink} placeholder="http://www.linkedin.com/"></input>
                                </div>
                             </div>
                          </div>
                          <div className="form-group">
                             <div className="">
                                <label className="sr-only">Site</label>
                                <div className="input-group mb-2">
                                   <div className="input-group-prepend">
                                       <div className="input-group-text"><FaTwitter /></div>
                                   </div>
                                   <input type="text" className="form-control"  onChange={handleTwitterLink} value={twitterLink}  placeholder="http://www.twitter.com/"></input>
                                </div>
                             </div>
                          </div>
                          <div className="form-group">
                             <div className="">
                                <label className="sr-only">Site</label>
                                <div className="input-group mb-2">
                                   <div className="input-group-prepend">
                                       <div className="input-group-text"><FaFacebook /></div>
                                   </div>
                                   <input type="text" className="form-control"  onChange={handleFacebookLink} value={facebookLink}  placeholder="http://www.facebook.com/"></input>
                                </div>
                             </div>
                          </div>
                          <div className="d-flex flex-row justify-content-end">
                              <div className="p-1">
                                  <button type="button" className="btn btn-outline-primary" onClick={handleHideInfo}>Annuler</button>
                              </div>
                              <div className="p-1">
                                   <Button type="submit" className="btn btn-primary" disabled={formSubmitting ? 'disabled': ''}>
                                      {formSubmitting ? <span className="font-weight-bold ">Sauvegarde...</span> : <span className="font-weight-bold ">Sauvegarder</span>}
                                   </Button>
                              </div>
                          </div>
                        </form>
                   </div>
                  } 

                  <div className="p-2" onMouseEnter={mouseEnterCompany} onMouseLeave={mouseLeaveCompany}> 
                      <div className="d-flex flex-row justify-content-between">
                        <div className="my-2"><b>Companies </b></div>
                        {showButtonAdd ? <div><MdAdd className="show-form-company" onClick={handleShowForm} /></div> : ''} 
                      </div>
                      <div className="d-flex follow">
                        <div className="p-2">No Content</div>
                      </div>
                   </div> 
                 </div>
                </div>
            </div>
          </Container>
          <Modal show={showForm} onHide={handleCloseForm}>
            <Modal.Header closeButton>
              <Modal.Title>Crée votre entrprise</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitCompany}>
            <Modal.Body>
                  <div className="d-flex flex-column">
                     {messageError ? <div className="p-2 justify-content-center"><h5 className="alert alert-danger">{messageError}</h5></div> : ''}
      
                     {errors ?
                     <div className="p-2 justify-content-center">
                        <ul className="list-group">
                           {errors.name ? <li className="list-group-item alert alert-danger">{errors.name}</li> : '' }
                           {errors.concept ? <li className="list-group-item alert alert-danger">{errors.concept}</li> : '' }
                           {errors.local ? <li className="list-group-item alert alert-danger">{errors.local}</li> : '' }
                           {errors.devises_id ? <li className="list-group-item alert alert-danger">{errors.devises_id}</li> : '' }
                           {errors.categories_id ? <li className="list-group-item alert alert-danger">{errors.categories_id}</li> : '' }
                           {errors.etapes_id ? <li className="list-group-item alert alert-danger">{errors.etapes_id}</li> : '' }
                        </ul>
                     </div>
                     :
                     ''
                     }

                     
                  </div>
                  <Form.Group>
                     <Form.Label>Nom Entreprise <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="text" onChange={handleCompanyName} />
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Concept <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  as="textarea" onChange={handleCompanyConcept} rows={3} />
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Localisation <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  type="text"  onChange={handleCompanyLocal} />
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Devise <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  as="select"  onChange={handleCompanyDevise}>
                     <option></option>
                     {devises.length !== 0 ?
                        devises.map((devise, index) => (
                           <option value={devise.id}>{devise.name}</option>
                        ))
                        :
                        <option></option>
                     }
                     </Form.Control>
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Categorie <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  as="select" onChange={handleCompanyCategory}>
                      <option></option>
                     {categories.length !== 0 ?
                        categories.map((category, index) => (
                           <option value={category.id}>{category.name}</option>
                        ))
                        :
                        <option></option>
                     }
                     </Form.Control>
                  </Form.Group>
                  <Form.Group>
                     <Form.Label>Stade de dévéloppment de l'entreprise <span style={{color: 'red'}}>*</span></Form.Label>
                     <Form.Control  as="select" onChange={handleCompanyEtape}>
                     <option></option>
                     {etapes.length !== 0 ?
                        etapes.map((etape, index) => (
                           <option value={etape.id}>{etape.name}</option>
                        ))
                        :
                        <option></option>
                     }
                     </Form.Control>
                  </Form.Group>
            </Modal.Body>
            <Modal.Footer>
             <Button variant="btn btn-outline-primary" onClick={handleCloseForm}>Annuler</Button>
             <Button type="submit">
               {formSubmitting ? <span className="font-weight-bold ">Enregistrement...</span> : <span className="font-weight-bold ">Enregistrer</span>}
             </Button>
            </Modal.Footer>
            </Form>
          </Modal>
          
          <Form style={{visibility: 'hidden'}} onSubmit={handleSubmitFile}>
             <Form.Group>
               <input type="file" ref={fileRef} name="file" onChange={() => handleUploadLogo(event)} />
             </Form.Group>
             <Button type="submit" ref={formRef} >Enregistrer</Button>
          </Form>
        </>
    )
}