import React from 'react';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import Home from './components/Home';
import Login from './views/Login/Login';
import SignUp from './components/auth/SignUp';
import SignIn from './components/auth/SignIn';
import ForgetPassword from './components/auth/ForgetPassword';
//import ForgetPassword from './components/auth/ForgetPassword';
import Register from './views/Register/Register';
import NotFound from './views/NotFound/NotFound';
import PrivateRoute from './PrivateRoute'
import Dashboard from './views/user/Dashboard';
import Profile from './views/user/Profile';
import Company from './views/user/Company';
import CompanyDetails from './views/user/CompanyDetails';
//import Dashboard from './views/user/Dashboard/Dashboard';
import Email from './views/password/Email';
import ResetPassword from './views/password/ResetPassword';
import Example from './components/Example';

const Main = props => (
  <Switch>
     <Route exact path='/' component={Home}/>  
     <Route path='/signin' component={SignIn}/>
     <Route path='/signup' component={SignUp}/>
     <Route path='/reset' component={Email}/>
     <Route path='/example' component={Example}/>
     <Route path='/forgot-password' component={ForgetPassword}/>
     <PrivateRoute path='/user/:id/profile' component={Profile}/>
     <PrivateRoute path='/user/:id/company' component={Company}/> 
     <PrivateRoute path='/user/:id/company/business_details' component={Company}/> 
     <PrivateRoute path='/test' component={Dashboard}/> 
     <Route  component={NotFound}/>
     
     {/* <PrivateRoute path='/dashboard' component={Dashboard}/> */}
  </Switch>);

export default Main;